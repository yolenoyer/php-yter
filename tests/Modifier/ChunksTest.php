<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class ChunksTest extends TestCase
{
    /**
     * @dataProvider chunksProvider
     */
    public function testChunks(
        array|Iterator $array,
        int $chunkSize,
        array $expectedResult,
    ) {
        $it = Iter::new($array);
        $chunksIt = $it->chunks($chunkSize);

        $this->assertSame($expectedResult, $chunksIt->toArray());
        $this->assertEmpty($it->toValues());
        $chunksIt->rewind();
        $this->assertSame($expectedResult, $chunksIt->toArray());
        $this->assertEmpty($it->toValues());
    }

    protected function chunksProvider(): array
    {
        return [
            'empty-data' => [ [], 3, [] ],
            'a' => [ [8], 1, [[8]] ],
            'b' => [ [8], 3, [[8]] ],
            'c' => [ [8, 9], 1, [[8], [9]] ],
            'd' => [ [8, 9], 2, [[8, 9]] ],
            'e' => [ [8, 9], 3, [[8, 9]] ],
            'f' => [ [8, 9, 10], 2, [[8, 9], [10]] ],
            'g' => [ [8, 9, 10, 11], 2, [[8, 9], [10, 11]] ],
            'h' => [ [8, 9, 10, 11, 12], 2, [[8, 9], [10, 11], [12]] ],
            'i' => [ [8, 9, 10, 11, 12], 3, [[8, 9, 10], [11, 12]] ],
            'j' => [
                iter([4, 7, 9, 6, 8, 1, 12, 14, 79, 78, 8])->filter(fn($v) => $v % 2 == 0),
                3,
                [ [4, 6, 8], [12, 14, 78], [8] ],
            ],
        ];
    }

    /**
     * @dataProvider chunksWithBadSizeProvider
     */
    public function testChunksWithBadSize(int $size)
    {
        $this->expectExceptionMessage("Negative or zero sizes are forbidden: $size");
        iter([1])->chunks($size);
    }

    protected function chunksWithBadSizeProvider(): array
    {
        return [
            'a' => [ 0 ],
            'b' => [ -1 ],
            'c' => [ -10 ],
        ];
    }
}
