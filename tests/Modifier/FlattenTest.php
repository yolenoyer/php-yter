<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Generator\Chars;
use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class FlattenTest extends TestCase
{
    /**
     * @dataProvider flattenProvider
     */
    public function testFlatten(
        array|Iterator $array,
        array $expectedResult,
    ) {
        $this->assertSame(
            $expectedResult,
            Iter::new($array)
                ->flatten()
                ->toValues()
        );
    }

    public function flattenProvider(): array
    {
        return [
            'empty-data' => [
                [],
                [],
            ],
            'empty-data-2' => [
                [[], []],
                [],
            ],
            'empty-data-3' => [
                [[], [], Iter::new([1, 5, 8])->filter(fn($n) => $n > 100)],
                [],
            ],
            'a' => [
                [[[]]],
                [[]],
            ],
            'b' => [
                [[], [[]], []],
                [[]],
            ],
            'c' => [
                [Chars::new('abc')],
                ['a', 'b', 'c'],
            ],
            'd' => [
                [Chars::new('abc')],
                ['a', 'b', 'c'],
            ],
            'e' => [
                [Chars::new('abc')->skip(2)],
                ['c'],
            ],
            'f' => [
                [
                    Chars::new('abc'),
                    Counter::sized(3),
                ],
                ['a', 'b', 'c', 0, 1, 2],
            ],
        ];
    }

    /**
     * @dataProvider flattenSkipProvider
     */
    public function testFlattenSkip(
        array|Iterator $array,
        int $nbSkips,
        array $expectedResult,
    ) {
        $this->assertSame(
            $expectedResult,
            Iter::new($array)
                ->flatten()
                ->skip($nbSkips)
                ->toValues()
        );
    }

    public function flattenSkipProvider(): array
    {
        return [
            [
                [
                    ['one', 'two', 'three'],
                    Counter::from(1, 5),
                ],
                2,
                ['three', 1, 2, 3, 4],
            ], [
                [
                    ['one', 'two', 'three'],
                    Counter::from(1, 5),
                ],
                3,
                [1, 2, 3, 4],
            ], [
                [
                    ['one', 'two', 'three'],
                    Counter::from(1, 5),
                ],
                4,
                [2, 3, 4],
            ],
        ];
    }
}
