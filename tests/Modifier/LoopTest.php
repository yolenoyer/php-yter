<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Generator\Chars;
use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Iter;
use Iterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;

class LoopTest extends TestCase
{
    /**
     * @dataProvider loopProvider
     */
    public function testLoop(
        iterable|callable $iterable,
        ?int $count,
        array $expectedResult,
    ) {
        $it = Iter::new($iterable)->loop($count);

        if (null === $count) {
            for ($i = 0; $i !== 100; ++$i) {
                foreach ($expectedResult as $value) {
                    $this->assertTrue($it->valid());
                    $this->assertSame($value, $it->current());
                    $it->next();
                }
            }
        } else {
            $this->assertSame($expectedResult, $it->toValues());
        }
    }

    public function loopProvider(): array
    {
        return [
            'empty-data' => [[], 0, []],
            'empty-data-2' => [[], 3, []],
            'a' => [['z'], 0, []],
            'b' => [['z'], 3, ['z', 'z', 'z']],
            'c' => [['z'], null, ['z', 'z', 'z']],
            'd' => [['z', 'a'], null, ['z', 'a', 'z', 'a']],
            'e' => [Counter::to(3), 2, [0, 1, 2, 0, 1, 2]],
            'f' => [
                Chars::new('àzèrtÿ')->slice(2, 3),
                3,
                ['è', 'r', 't', 'è', 'r', 't', 'è', 'r', 't'],
            ],
        ];
    }

    public function testEmptyInfiniteLoop()
    {
        $it = Iter::new([])->loop();

        $this->assertFalse($it->valid());
    }
}
