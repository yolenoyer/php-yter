<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class SliceTest extends TestCase
{
    /**
     * @dataProvider sliceProvider
     */
    public function testSlice(
        array|Iterator $array,
        int $start,
        ?int $length,
        array $expectedResult,
    ) {
        $it = Iter::new($array);

        $this->assertSame($expectedResult, $it->slice($start, $length)->toValues());
        $it->rewind();
        $this->assertSame($expectedResult, $it->slice($start, $length)->toValues());
    }

    public function sliceProvider(): array
    {
        return [
            'empty-data' => [[], 0, 0, []],
            'empty-data-2' => [[], 0, null, []],
            'empty-data-3' => [[], 0, 10, []],
            'empty-data-4' => [[], 10, 10, []],
            'empty-data-5' => [[], 10, null, []],
            'a' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 0, 0, []],
            'b' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 5, 0, []],
            'c' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 10, 0, []],
            'd1' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 0, 1, ['a']],
            'e1' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 0, 2, ['a', 'b']],
            'f1' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 0, 7, ['a', 'b', 'c', 'd', 'e', 'f', 'g']],
            'g1' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 0, 10, ['a', 'b', 'c', 'd', 'e', 'f', 'g']],
            'h1' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 0, null, ['a', 'b', 'c', 'd', 'e', 'f', 'g']],
            'd2' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 1, 1, ['b']],
            'e2' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 1, 2, ['b', 'c']],
            'f2' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 1, 6, ['b', 'c', 'd', 'e', 'f', 'g']],
            'g2' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 1, 10, ['b', 'c', 'd', 'e', 'f', 'g']],
            'h2' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 1, null, ['b', 'c', 'd', 'e', 'f', 'g']],
            'd3' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 6, 1, ['g']],
            'e3' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 6, 2, ['g']],
            'f3' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 6, 6, ['g']],
            'h3' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 6, null, ['g']],
            'd4' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 10, 1, []],
            'h4' => [['a', 'b', 'c', 'd', 'e', 'f', 'g'], 10, null, []],
        ];
    }
}
