<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Iter;
use Iterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;

class MapKeysTest extends TestCase
{
    /**
     * @dataProvider mapKeysProvider
     */
    public function testMapKeys(
        iterable|callable $iterable,
        callable $keyMapper,
        array $expectedResult,
    ) {
        $it = Iter::new($iterable)->mapKeys($keyMapper);

        $this->assertSame($expectedResult, $it->toArray());
    }

    public function mapKeysProvider(): array
    {
        return [
            'empty-data' => [[], fn() => null, []],
            'a' => [['a'], fn($k) => $k, ['a']],
            'b' => [
                ['a', 'b'],
                fn($k) => $k * 2,
                ['a', 2 => 'b'],
            ],
            'c' => [
                ['foo' => 'bar'],
                fn($k) => strtoupper($k),
                ['FOO' => 'bar'],
            ],
            'd' => [
                ['foo' => 'bar', 'x' => 78],
                fn($k) => strtoupper($k),
                ['FOO' => 'bar', 'X' => 78],
            ],
            'e' => [
                Iter::new(['foo' => 'bar', 'x' => 78, 'y' => 13, 'z' => 777])->slice(1, 2),
                fn($k) => strtoupper($k),
                ['X' => 78, 'Y' => 13],
            ],
        ];
    }
}
