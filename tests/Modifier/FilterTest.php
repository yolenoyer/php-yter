<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Iter;
use Dajoha\Iter\Modifier\Filter;
use Iterator;
use PHPUnit\Framework\TestCase;

class FilterTest extends TestCase
{
    /**
     * @dataProvider filterProvider
     */
    public function testFilter(
        array|Iterator $array,
        callable $filter,
        array $expectedResult,
    ) {
        $it = Iter::new($array);
        $filterIt = $it->filter($filter);

        $this->assertSame($expectedResult, $filterIt->toValues());
        $this->assertEmpty($it->toValues());
        $filterIt->rewind();
        $this->assertSame($expectedResult, $filterIt->toValues());
        $this->assertEmpty($it->toValues());
        $it->rewind();
        $this->assertSame($expectedResult, $it->filter($filter)->toValues());
        $this->assertEmpty($it->toValues());
    }

    protected function filterProvider(): array
    {
        return [
            'empty-data' => [
                [],
                fn($s) => strlen($s) === 5,
                [],
            ],
            'a' => [
                ['hi', true, 'big', 'world', 489],
                fn() => true,
                ['hi', true, 'big', 'world', 489],
            ],
            'b' => [
                ['hi', true, 'big', 'world', 489],
                fn() => false,
                [],
            ],
            'c' => [
                ['hi', 'hello', 'big', 'world', 'z'],
                fn($s) => strlen($s) === 5,
                ['hello', 'world'],
            ],
            'd' => [
                ['hi', 'hello', 'big', 'world', 'z'],
                fn($s) => str_starts_with($s, 'h'),
                ['hi', 'hello'],
            ],
            'e' => [
                Counter::to(8),
                fn($n) => $n % 2 === 0,
                [0, 2, 4, 6],
            ],
            'f' => [
                Filter::new([10, 20, 30, 40, 50, 60], fn($n) => $n !== 30),
                fn($n) => $n < 55,
                [10, 20, 40, 50],
            ],
            'g' => [
                ['a' => 10, 'b' => 20, 'c' => 30],
                fn($v, $k) => $k != 'b',
                [10, 30],
            ],
        ];
    }
}
