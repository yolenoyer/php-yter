<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class LimitTest extends TestCase
{
    /**
     * @dataProvider limitProvider
     */
    public function testLimit(
        array|Iterator $array,
        int $limit,
        array $expectedResult,
    ) {
        $it = Iter::new($array);

        $this->assertSame($expectedResult, $it->limit($limit)->toValues());
        $it->rewind();
        $this->assertSame($expectedResult, $it->limit($limit)->toValues());
    }

    public function limitProvider(): array
    {
        return [
            'empty-data' => [
                [],
                0,
                []
            ],
            'empty-data-2' => [
                [],
                1,
                []
            ],
            'empty-data-3' => [
                [],
                10,
                []
            ],
            'a' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                0,
                [],
            ],
            'b' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                1,
                ['a'],
            ],
            'c' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                4,
                ['a', 'b', 'c', 'd'],
            ],
            'd' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                7,
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
            ],
            'e' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                8,
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
            ],
            'f' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                70,
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
            ],
        ];
    }

    public function testNullLimit()
    {
        $it = Counter::new()->limit(null);
        for ($i = 0; $i !== 20; ++$i) {
            $this->assertTrue($it->valid());
            $this->assertSame($i, $it->current());
            $it->next();
        }
    }
}
