<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Iter;
use PHPUnit\Framework\TestCase;

class RunTest extends TestCase
{
    public function testRun()
    {
        $sum = 0;
        $it = Iter::new([1, 2, 3])
            ->run(function ($n) use (&$sum) { $sum += $n; })
            ->map(fn($n) => $n * 2);
        $values = $it->toValues();

        $this->assertSame(6, $sum);
        $this->assertSame([2, 4, 6], $values);
    }
}
