<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class FilterKeysTest extends TestCase
{
    /**
     * @dataProvider filterKeysProvider
     */
    public function testFilterKeys(
        array|Iterator $array,
        callable $filter,
        array $expectedResult,
    ) {
        $it = Iter::new($array);
        $filterKeysIt = $it->filterKeys($filter);

        $this->assertSame($expectedResult, $filterKeysIt->toArray());
        $this->assertEmpty($it->toArray());
        $filterKeysIt->rewind();
        $this->assertSame($expectedResult, $filterKeysIt->toArray());
        $this->assertEmpty($it->toArray());
        $it->rewind();
        $this->assertSame($expectedResult, $it->filterKeys($filter)->toArray());
        $this->assertEmpty($it->toArray());
    }

    protected function filterKeysProvider(): array
    {
        return [
            'empty-data' => [
                [],
                fn($k) => strlen($k) === 5,
                [],
            ],
            'a' => [
                ['hi', true, 'name' => 'big', 'world', 489],
                fn() => true,
                ['hi', true, 'name' => 'big', 'world', 489],
            ],
            'b' => [
                ['hi', true, 'name' => 'big', 'world', 489],
                fn() => false,
                [],
            ],
            'c' => [
                ['hi', 'hello', 'big', 'world', 'z'],
                fn($k) => $k < 2,
                ['hi', 'hello'],
            ],
            'd' => [
                [ 'name' => 'John', 'age' => 25 ],
                fn($k) => $k === 'age',
                [ 'age' => 25 ],
            ],
            'e' => [
                [ 'name' => 'John', 'age' => 25, 'sex' => 'm' ],
                fn($k) => strlen($k) === 3,
                [ 'age' => 25, 'sex' => 'm' ],
            ],
            'f' => [
                [ 'name' => 'John', 'age' => 25, 'sex' => 'm' ],
                fn($k, $v) => $k == 'age' || $v == 'm',
                [ 'age' => 25, 'sex' => 'm'],
            ],
        ];
    }
}
