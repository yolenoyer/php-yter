<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class SkipTest extends TestCase
{
    /**
     * @dataProvider skipProvider
     */
    public function testSkip(
        array|Iterator $array,
        int $skip,
        array $expectedResult,
    ) {
        $it = Iter::new($array);
        $skipIt = $it->skip($skip);

        $this->assertSame($expectedResult, $skipIt->toValues());
        $skipIt->rewind();
        $this->assertSame($expectedResult, $skipIt->toValues());
        $it->rewind();
        $this->assertSame($expectedResult, $it->skip($skip)->toValues());
    }

    public function skipProvider(): array
    {
        return [
            'empty-data' => [
                [],
                0,
                []
            ],
            'empty-data-2' => [
                [],
                1,
                []
            ],
            'empty-data-3' => [
                [],
                10,
                []
            ],
            'a' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                0,
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
            ],
            'b' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                1,
                ['b', 'c', 'd', 'e', 'f', 'g'],
            ],
            'c' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                4,
                ['e', 'f', 'g'],
            ],
            'd' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                7,
                [],
            ],
            'e' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                8,
                [],
            ],
            'f' => [
                ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                70,
                [],
            ],
        ];
    }
}
