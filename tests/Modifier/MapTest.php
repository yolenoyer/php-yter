<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Iter;
use Iterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;

class MapTest extends TestCase
{
    /**
     * @dataProvider mapProvider
     */
    public function testMap(
        iterable|callable $iterable,
        callable $mapper,
        array $expectedResult,
    ) {
        $it = Iter::new($iterable)->map($mapper);

        $this->assertSame($expectedResult, $it->toValues());
    }

    public function mapProvider(): array
    {
        return [
            'empty-data' => [[], fn() => null, []],
            'a' => [[1], fn($n) => $n, [1]],
            'b' => [[1, 4, 'foo'], fn($n) => $n, [1, 4, 'foo']],
            'c' => [[1, 4, 7], fn($n) => $n * 2, [2, 8, 14]],
            'd' => [
                Counter::from(1, 4)->loop(2),
                fn($n) => $n * $n,
                [1, 4, 9, 1, 4, 9],
            ],
        ];
    }
}
