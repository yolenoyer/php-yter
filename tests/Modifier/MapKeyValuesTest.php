<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Modifier;

use Dajoha\Iter\Iter;
use Iterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;

class MapKeyValuesTest extends TestCase
{
    /**
     * @dataProvider mapKeysProvider
     */
    public function testMapKeyValues(
        iterable|callable $iterable,
        callable $keyValueMapper,
        array $expectedResult,
    ) {
        $it = Iter::new($iterable)->mapKeyValues($keyValueMapper);

        $this->assertSame($expectedResult, $it->toArray());
    }

    public function mapKeysProvider(): array
    {
        return [
            'empty-data' => [[], fn() => null, []],
            'a' => [['a'], fn($k, $v) => [$k, $v], ['a']],
            'b' => [
                ['a', 'b'],
                fn($k, $v) => [$k * 2, $v],
                ['a', 2 => 'b'],
            ],
            'c' => [
                ['foo' => 'bar'],
                fn($k, $v) => [strtoupper($k), "$v$v"],
                ['FOO' => 'barbar'],
            ],
            'd' => [
                ['foo' => 'bar', 'x' => 78],
                fn($k) => [strtoupper($k), null],
                ['FOO' => null, 'X' => null],
            ],
            'e' => [
                iter(['foo' => 'bar', 'x' => 78, 'y' => 13, 'z' => 777])->slice(1, 2),
                fn($k, $v) => [strtoupper($k), $v * 2],
                ['X' => 156, 'Y' => 26],
            ],
            'g' => [
                iter(['two' => 2, 'four' => 4, 'seven' => 7]),
                fn($k, $v) => [$v, $k],
                [2 => 'two', 4 => 'four', 7 => 'seven'],
            ],
            'h' => [
                iter(['two' => 'FOO', 'four' => 'FOO']),
                fn($k, $v) => [$v, $k],
                ['FOO' => 'four'],
            ],
        ];
    }
}
