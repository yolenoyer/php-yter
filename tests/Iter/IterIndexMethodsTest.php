<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\Exception\IterException;
use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

final class IterIndexMethodsTest extends TestCase
{
    /**
     * @dataProvider iterIndexMethodsProvider
     * @throws IterException
     */
    public function testIterIndexMethods(
        array|Iterator $array,
        mixed $expectedFirst,
        mixed $expectedLast,
        NthExpect ...$nthExpects,
    ) {
        $it = Iter::new($array);

        $this->assertSame($it->first(), $expectedFirst);
        $this->assertSame($it->first(), $expectedFirst);
        $this->assertSame($it->nth(0), $expectedFirst);
        $this->assertSame($it->nth(0), $expectedFirst);
        $this->assertSame($it->last(), $expectedLast);

        foreach ($nthExpects as $nthExpect) {
            $it->rewind();
            $this->assertSame($nthExpect->value, $it->nth($nthExpect->index));
        }
    }

    public function iterIndexMethodsProvider(): array
    {
        $expect = fn($index, $value) => new NthExpect($index, $value);

        return [
            'empty-data' => [
                [],
                null,
                null,
                $expect(0, null),
                $expect(1, null),
                $expect(10, null),
            ],
            'a' => [
                [7],
                7,
                7,
                $expect(0, 7),
                $expect(1, null),
                $expect(10, null),
            ],
            'b' => [
                [7, 13, 21],
                7,
                21,
                $expect(0, 7),
                $expect(1, 13),
                $expect(2, 21),
                $expect(10, null),
            ],
            'c' => [
                Iter::new(['a', 'zzzz', 'fgh', 'tyui', 'ff', 'klmn'])
                    ->filter(fn($s) => strlen($s) === 4),
                'zzzz',
                'klmn',
                $expect(0, 'zzzz'),
                $expect(1, 'tyui'),
                $expect(2, 'klmn'),
                $expect(10, null),
            ],
            'd' => [
                Iter::new(['a', 'zzzz', 'fgh', 'tyui', 'ff', 'klmn'])
                    ->slice(1, 3),
                'zzzz',
                'tyui',
                $expect(0, 'zzzz'),
                $expect(1, 'fgh'),
                $expect(2, 'tyui'),
                $expect(3, null),
                $expect(10, null),
            ],
        ];
    }

    /**
     * @throws IterException
     */
    public function testIterNthWithNegativeIndex()
    {
        $this->expectExceptionMessageMatches('/Negative indices are forbidden/');

        $it = Iter::new([4, 7]);
        $it->nth(-2);
    }
}

class NthExpect {
    public function __construct(public int $index, public mixed $value)
    {
    }
}
