<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

final class IterNumericMethodsTest extends TestCase
{
    /**
     * @dataProvider iterNumericMethodsProvider
     */
    public function testIterNumericMethods(
        array|Iterator $array,
        null|int|float $expectedMin,
        null|int|float $expectedMax,
        int|float $expectedSum,
        null|int|float $expectedAverage,
    ): void
    {
        $it = Iter::new($array);
        $this->assertSame($expectedMin, $it->min());
        $it->rewind();
        $this->assertSame($expectedMax, $it->max());
        $it->rewind();
        $this->assertSame($expectedSum, $it->sum());
        $it->rewind();
        $this->assertSame($expectedAverage, $it->average());
    }

    public function iterNumericMethodsProvider(): array
    {
        return [
            [ []               , null, null,   0, null ],
            [ [0]              ,    0,    0,   0,    0 ],
            [ [2]              ,    2,    2,   2,    2 ],
            [ [-7]             ,   -7,   -7,  -7,   -7 ],
            [ [0, 0, 0]        ,    0,    0,   0,    0 ],
            [ [0, 0, 6]        ,    0,    6,   6,    2 ],
            [ [-12, 0, 6]      ,  -12,    6,  -6,   -2 ],
            [ [20, 30, 50, 100],   20,  100, 200,   50 ],
            [
                Iter::new([20, 30, 50, 100])->slice(1, 2),
                30,
                50,
                80,
                40,
            ],
        ];
    }
}
