<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\AbstractIterator;
use Dajoha\Iter\Iter;
use Dajoha\Iter\Modifier\Map;
use Dajoha\Iter\Traits\InnerIteratorTrait;
use Iterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;

class IterApplyTest extends TestCase
{
    public function testIterApply()
    {
        $this->assertSame(
            [10, 18, 26],
            Iter::new([5, 9, 13])
                ->apply(Double::class)
                ->toValues(),
        );

        $this->assertSame(
            [15, 27, 39],
            Iter::new([5, 9, 13])
                ->apply(Multiply::class, 3)
                ->toValues(),
        );
    }
}

class Double extends AbstractIterator {
    use InnerIteratorTrait;

    public function __construct(iterable|callable $iterable)
    {
        $this->setInnerIterator(new Map($iterable, fn($v) => $v * 2));
    }
}

class Multiply extends AbstractIterator {
    use InnerIteratorTrait;

    public function __construct(iterable|callable $iterable, int|float $factor)
    {
        $this->setInnerIterator(new Map($iterable, fn($v) => $v * $factor));
    }
}
