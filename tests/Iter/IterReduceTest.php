<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class IterReduceTest extends TestCase
{
    /**
     * @dataProvider iterReduceProvider
     */
    public function testIterReduce(
        array|Iterator $array,
        callable $reducer,
        mixed $initialValue,
        mixed $expectedResult,
    ) {
        $reduced = Iter::new($array)->reduce($reducer, $initialValue);
        $this->assertSame($expectedResult, $reduced);
    }

    public function iterReduceProvider(): array
    {
        return [
            'empty-data' => [
                [],
                fn($acc, $current) => $acc + $current,
                7,
                7,
            ],
            'a' => [
                ['o w', 'or', 'ld'],
                fn($acc, $current) => $acc.$current,
                'hell',
                'hello world',
            ],
            'b' => [
                [10, 20, 30],
                fn($acc, $current) => $acc + $current,
                0,
                60,
            ],
            'c' => [
                Iter::new([10, 20, 30])
                    ->map(fn($n) => $n * 2),
                fn($acc, $current) => $acc + $current,
                1,
                121,
            ],
            'd' => [
                Iter::new([10, 20, 30])
                    ->filter(fn($n) => $n != 20)
                    ->map(fn($n) => $n * 2),
                fn($acc, $current) => $acc + $current,
                1,
                81,
            ],
            'factorial' => [
                Counter::from(1, 7),
                fn($acc, $current) => $acc * $current,
                1,
                720,
            ],
        ];
    }
}
