<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\Iter;
use Iterator;
use PHPUnit\Framework\TestCase;

class IterJoinTest extends TestCase
{
    /**
     * @dataProvider iterJoinProvider
     */
    public function testIterJoin(array|Iterator $array, string $expectedResult)
    {
        $this->assertSame($expectedResult, Iter::new($array)->join());
    }

    public function iterJoinProvider(): array
    {
        return [
            [
                [],
                '',
            ], [
                ['!'],
                '!',
            ], [
                ['!', '?'],
                '!?',
            ], [
                ['Hello', ' ', 'world'],
                'Hello world',
            ], [
                ['Hello', ' ', 12],
                'Hello 12',
            ], [
                Iter::new(['a', 'b', 'Hello', ' ', 12])->skip(2),
                'Hello 12',
            ], [
                Iter::new(['a', 'b', 'c', 'd', 'e', 'f', 'g'])->slice(2, 4),
                'cdef',
            ],
        ];
    }

    /**
     * @dataProvider iterJoinWithSeparatorProvider
     */
    public function testIterJoinWithSeparator(
        array|Iterator $array,
        string $separator,
        string $expectedResult,
    ) {
        $this->assertSame($expectedResult, Iter::new($array)->join($separator));
    }

    public function iterJoinWithSeparatorProvider(): array
    {
        return [
            'empty-data' => [
                [],
                '',
                '',
            ],
            'empty-data_with-sep' => [
                [],
                ',,',
                '',
            ],
            'a' => [
                ['!'],
                '',
                '!',
            ],
            'b' => [
                ['!'],
                ',,',
                '!',
            ],
            'c' => [
                ['!', '?'],
                ',,',
                '!,,?',
            ],
            'd' => [
                ['Hello', 'world'],
                ', ',
                'Hello, world',
            ],
            'e' => [
                ['Hello', ' ', 12],
                ' !! ',
                'Hello !!   !! 12',
            ],
        ];
    }

    /**
     * @dataProvider iterJoinWithLastSeparatorProvider
     */
    public function testIterJoinWithLastSeparator(
        array|Iterator $array,
        string $separator,
        string $lastSeparator,
        string $expectedResult,
    ) {
        $this->assertSame($expectedResult, Iter::new($array)->join($separator, $lastSeparator));
    }

    public function iterJoinWithLastSeparatorProvider(): array
    {
        return [
            'empty-data' => [
                [],
                '',
                '',
                '',
            ],
            'empty-data_with-sep' => [
                [],
                ',,',
                '!!',
                '',
            ],
            'a' => [
                ['!'],
                'a',
                'b',
                '!',
            ],
            'b' => [
                ['!'],
                ',,',
                '!!',
                '!',
            ],
            'c' => [
                ['Hello', 'world'],
                ', ',
                ' and ',
                'Hello and world',
            ],
            'd' => [
                ['Hello', 12, 'foo', 'bar'],
                ' !! ',
                ' :: ',
                'Hello !! 12 !! foo :: bar',
            ],
        ];
    }
}
