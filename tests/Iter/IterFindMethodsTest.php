<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\Iter;
use PHPUnit\Framework\TestCase;

final class IterFindMethodsTest extends TestCase
{
    /**
     * @dataProvider iterFindMethodsProvider
     */
    public function testIterFindMethods(array $array, FindExpect ...$findExpects)
    {
        $it = Iter::new($array);

        // Test `findValue()` method:
        foreach ($findExpects as $findExpect) {
            $this->assertSame($findExpect->match, $it->find($findExpect->predicate, $found));
            $this->assertSame($findExpect->found, $found);
        }

        $it->rewind();

        // Test `find()` method:
        foreach ($findExpects as $findExpect) {
            $this->assertSame($findExpect->found, $it->isFound($findExpect->predicate, $value));
            $this->assertSame($findExpect->match, $value);
        }
    }

    public function testIterConsecutiveFindMethods()
    {
        $it = Iter::new([0, -2, 5, -3]);
        $predicate = fn($n) => $n < 0;

        $this->assertSame(-2, $it->find($predicate));
        $this->assertSame(-3, $it->find($predicate));
        $this->assertSame(null, $it->find($predicate));

        $it->rewind();

        $this->assertSame(true, $it->isFound($predicate, $value, $key));
        $this->assertSame([1, -2], [$key, $value]);
        $this->assertSame(true, $it->isFound($predicate, $value, $key));
        $this->assertSame([3, -3], [$key, $value]);
        $this->assertSame(false, $it->isFound($predicate, $value, $key));
        $this->assertSame([null, null], [$key, $value]);
    }

    public function iterFindMethodsProvider(): array
    {
        $expectMatch = fn($predicate, $match) => new FindExpect($predicate, true, $match);
        $expectNoMatch = fn($predicate) => new FindExpect($predicate, false, null);

        $isOdd = fn($n) => $n % 2 === 1;
        $isEven = fn($n) => $n % 2 === 0;
        $isGreaterThan100 = fn($n) => $n > 100;
        $isNull = fn($v) => null === $v;

        return [
            'empty-data' => [
                [],
                $expectNoMatch($isOdd),
                $expectNoMatch($isOdd),
            ],
            'a' => [
                [10],
                $expectNoMatch($isOdd),
                $expectNoMatch($isOdd),
            ],
            'b' => [
                [11],
                $expectMatch($isOdd, 11),
                $expectNoMatch($isOdd),
            ],
            'c' => [
                [10, 11],
                $expectMatch($isOdd, 11),
                $expectNoMatch($isOdd),
            ],
            'd' => [
                [11, 17],
                $expectMatch($isOdd, 11),
                $expectMatch($isOdd, 17),
            ],
            'e' => [
                [10, 201],
                $expectMatch($isEven, 10),
                $expectMatch($isGreaterThan100, 201),
            ],
            'f' => [
                [10, 201],
                $expectMatch($isGreaterThan100, 201),
                $expectNoMatch($isEven),
                $expectNoMatch($isEven),
            ],
            'g' => [
                [10, null],
                $expectMatch($isNull, null),
                $expectNoMatch($isNull),
            ],
            'h' => [
                [10, null, 11, null],
                $expectMatch($isNull, null),
                $expectMatch($isNull, null),
                $expectNoMatch($isNull),
            ],
            'i' => [
                ['hello', 10, null, 11, null],
                $expectMatch($isNull, null),
                $expectMatch($isOdd, 11),
                $expectMatch($isNull, null),
                $expectNoMatch($isNull),
            ],
        ];
    }
}

class FindExpect {
    public function __construct(
        /** @var callable $predicate */
        public $predicate,
        public bool $found,
        public mixed $match,
    ) {
    }
}
