<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\Generator\AsciiChars;
use Dajoha\Iter\Iter;
use Dajoha\Iter\Tests\TestUtil;
use Iterator;
use PHPUnit\Framework\TestCase;

class IterAnyTest extends TestCase
{
    public function testIterAnyWithEmptyIterator()
    {
        $this->assertFalse(Iter::new([])->any(fn($n) => true));
    }

    /**
     * @dataProvider iterAnyProvider
     */
    public function testIterAny(array|Iterator $array, callable $anyPredicate, bool $expectedReturn = true)
    {
        $it = Iter::new($array);

        $this->assertSame($expectedReturn, $it->any($anyPredicate));
        $it->rewind();
        $this->assertSame(!$expectedReturn, $it->all(TestUtil::negatePredicate($anyPredicate)));
    }

    /**
     * @dataProvider iterConsecutiveAnyProvider
     */
    public function testIterConsecutiveAny(
        array|Iterator $array,
        callable $anyPredicate,
        bool $expectedReturn1 = true,
        bool $expectedReturn2 = true,
    ) {
        $it = Iter::new($array);

        $this->assertSame($expectedReturn1, $it->any($anyPredicate));
        $this->assertSame($expectedReturn2, $it->any($anyPredicate));
    }

    public function iterAnyProvider(): array
    {
        return [
            'empty-data' => [
                [],
                fn($n) => true,
                false,
            ],
            'a' => [
                [10, 20, 30, 77],
                fn($n) => $n % 2 === 0,
            ],
            'b' => [
                [10, 20, 30],
                fn($n) => $n < 15,
            ],
            'c' => [
                [10, 20, 30],
                fn($n) => $n > 25,
            ],
            'd' => [
                [11, 21, 33, 77],
                fn($n) => $n % 2 === 0,
                false,
            ],
            'e' => [
                [30, 50, 70],
                fn($n) => $n < 15,
                false,
            ],
            'f' => [
                [0, 10, 20],
                fn($n) => $n > 25,
                false,
            ],
            'g' => [
                AsciiChars::new('hello'),
                fn($c) => $c === 'o',
            ],
            'h' => [
                AsciiChars::new('hello'),
                fn($c) => $c === '?',
                false,
            ],
            'i' => [
                AsciiChars::new('hello')
                    ->filter(fn($c) => $c !== 'e'),
                fn($c) => $c !== 'e',
            ],
        ];
    }

    public function iterConsecutiveAnyProvider(): array
    {
        return [
            'empty-data' => [
                [],
                fn($n) => true,
                false,
                false,
            ],
            'a' => [
                [10, 20, 30, 77],
                fn($n) => $n % 2 === 0,
            ],
            'b' => [
                [10, 20, 30],
                fn($n) => $n < 15,
                true,
                false,
            ],
            'c' => [
                [10, 20, 30],
                fn($n) => $n > 25,
                true,
                false,
            ],
            'd' => [
                [11, 21, 33, 77],
                fn($n) => $n % 2 === 0,
                false,
                false,
            ],
            'e' => [
                [30, 50, 70],
                fn($n) => $n < 15,
                false,
                false,
            ],
            'f' => [
                [0, 10, 20],
                fn($n) => $n > 25,
                false,
                false,
            ],
            'g' => [
                AsciiChars::new('hello'),
                fn($c) => $c === 'o',
                true,
                false,
            ],
            'h' => [
                AsciiChars::new('hello'),
                fn($c) => $c === '?',
                false,
                false,
            ],
            'i' => [
                AsciiChars::new('hello')
                    ->filter(fn($c) => $c !== 'e'),
                fn($c) => $c !== 'e',
            ],
        ];
    }
}
