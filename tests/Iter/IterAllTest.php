<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use Dajoha\Iter\Generator\AsciiChars;
use Dajoha\Iter\Iter;
use Dajoha\Iter\Tests\TestUtil;
use Iterator;
use PHPUnit\Framework\TestCase;

class IterAllTest extends TestCase
{
    public function testIterAllWithEmptyIterator()
    {
        $this->assertTrue(Iter::new([])->all(fn($n) => true));
    }

    /**
     * @dataProvider iterAllProvider
     */
    public function testIterAll(array|Iterator $array, callable $allPredicate, bool $expectedReturn = true)
    {
        $it = Iter::new($array);

        $this->assertSame($expectedReturn, $it->all($allPredicate));
        $it->rewind();
        $this->assertSame(!$expectedReturn, $it->any(TestUtil::negatePredicate($allPredicate)));
    }

    /**
     * @dataProvider iterConsecutiveAllProvider
     */
    public function testIterConsecutiveAll(
        array|Iterator $array,
        callable $allPredicate,
        bool $expectedReturn1 = true,
        bool $expectedReturn2 = true,
    ) {
        $it = Iter::new($array);

        $this->assertSame($expectedReturn1, $it->all($allPredicate));
        $this->assertSame($expectedReturn2, $it->all($allPredicate));
    }

    public function iterAllProvider(): array
    {
        return [
            'empty-data' => [
                [],
                fn($n) => true,
            ],
            'a' => [
                [10, 20, 30],
                fn($n) => $n % 2 === 0,
            ],
            'b' => [
                [10, 20, 30],
                fn($n) => $n > 5,
            ],
            'c' => [
                [10, 20, 30],
                fn($n) => $n < 100,
            ],
            'd' => [
                [10, 21, 30],
                fn($n) => $n % 2 === 0,
                false,
            ],
            'e' => [
                [0, 10, 20, 30],
                fn($n) => $n > 5,
                false,
            ],
            'f' => [
                [1300, 10, 20, 30],
                fn($n) => $n < 100,
                false,
            ],
            'g' => [
                AsciiChars::new('hello'),
                fn($c) => $c !== '?',
            ],
            'h' => [
                AsciiChars::new('hello'),
                fn($c) => $c !== 'e',
                false,
            ],
            'i' => [
                AsciiChars::new('hello')
                    ->filter(fn($c) => $c !== 'e'),
                fn($c) => $c !== 'e',
            ],
        ];
    }

    public function iterConsecutiveAllProvider(): array
    {
        return [
            'empty-data' => [
                [],
                fn($n) => true,
            ],
            'a' => [
                [10, 20, 30],
                fn($n) => $n % 2 === 0,
                true,
                true,
            ],
            'b' => [
                [10, 20, 30],
                fn($n) => $n > 5,
                true,
                true,
            ],
            'c' => [
                [10, 20, 30],
                fn($n) => $n < 100,
                true,
                true,
            ],
            'd' => [
                [10, 21, 30],
                fn($n) => $n % 2 === 0,
                false,
                true,
            ],
            'e' => [
                [0, 10, 20, 30],
                fn($n) => $n > 5,
                false,
                true,
            ],
            'f' => [
                [1300, 10, 20, 30],
                fn($n) => $n < 100,
                false,
                true,
            ],
            'g' => [
                AsciiChars::new('hello'),
                fn($c) => $c !== '?',
            ],
            'h' => [
                AsciiChars::new('hello'),
                fn($c) => $c !== 'e',
                false,
                true,
            ],
            'i' => [
                AsciiChars::new('hello')
                    ->filter(fn($c) => $c !== 'e'),
                fn($c) => $c !== 'e',
            ],
        ];
    }
}
