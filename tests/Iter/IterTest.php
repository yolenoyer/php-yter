<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Iter;

use ArrayIterator;
use Dajoha\Iter\Iter;
use Dajoha\Iter\IteratorInterface;
use Iterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;
use Traversable;

final class IterTest extends TestCase
{
    /**
     * @dataProvider iterFromArrayProvider
     */
    public function testIterFromArray(
        IteratorInterface $it,
        array $expectedArray,
        int $expectedCount,
        string $expectedJoin,
    ): void
    {
        $this->assertSame($expectedArray, $it->toValues());
        $it->rewind();
        $this->assertSame($expectedCount, $it->count());
        $it->rewind();
        $this->assertSame($expectedJoin, $it->join());
    }

    public function iterFromArrayProvider(): array
    {
        return [
            'empty-data' => [
                Iter::new([]),
                [],
                0,
                '',
            ],
            'a' => [
                Iter::new(Iter::new([])),
                [],
                0,
                '',
            ],
            'b' => [
                Iter::new([10]),
                [10],
                1,
                '10',
            ],
            'c' => [
                Iter::new(Iter::new([10])),
                [10],
                1,
                '10',
            ],
            'd' => [
                Iter::new(['hello', 'world']),
                ['hello', 'world'],
                2,
                'helloworld',
            ],
            'e' => [
                Iter::new(Iter::new(['hello', 'world'])),
                ['hello', 'world'],
                2,
                'helloworld',
            ],
            'f' => [
                Iter::new(['one', 'two', 'hello', 'world'])->skip(2),
                ['hello', 'world'],
                2,
                'helloworld',
            ],
            'g' => [
                Iter::new(new class implements Iterator {
                    protected int $i = 3;

                    public function current(): ?int
                    {
                        return $this->i >= 0 ? $this->i : null;
                    }

                    public function next(): void
                    {
                        $this->i--;
                    }

                    public function key(): mixed
                    {
                        return null;
                    }

                    public function valid(): bool
                    {
                        return $this->i >= 0;
                    }

                    public function rewind(): void
                    {
                        $this->i = 3;
                    }
                }),
                [3, 2, 1, 0],
                4,
                '3210',
            ],
            'h' => [
                Iter::new(new class implements IteratorAggregate {
                    public function getIterator(): Traversable
                    {
                        return new ArrayIterator([3, 2, 1, 0]);
                    }
                }),
                [3, 2, 1, 0],
                4,
                '3210',
            ],
        ];
    }

    public function testConsume()
    {
        $n = 0;
        iter(['foo', 'bar'])
            ->run(function() use (&$n) { ++$n; })
            ->consume()
        ;
        $this->assertSame(2, $n);
    }
}
