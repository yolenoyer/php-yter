<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests;

class TestUtil
{
    public static function negatePredicate(callable $predicate): callable
    {
        return fn(...$args) => !$predicate(...$args);
    }
}
