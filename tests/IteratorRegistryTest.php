<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests;

use Dajoha\Iter\AbstractIterator;
use Dajoha\Iter\Iter;
use Dajoha\Iter\IteratorRegistry;
use Dajoha\Iter\Modifier\Map;
use Dajoha\Iter\Traits\InnerIteratorTrait;
use Iterator;
use IteratorAggregate;
use PHPUnit\Framework\TestCase;

class IteratorRegistryTest extends TestCase
{
    /**
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpUndefinedMethodInspection
     */
    public function testRegister()
    {
        IteratorRegistry::clear();
        IteratorRegistry::register('double', Double::class);
        IteratorRegistry::register('multiply', Multiply::class);

        $this->assertSame(
            [10, 18, 26],
            Iter::new([5, 9, 13])
                ->double()
                ->toValues(),
        );

        $this->assertSame(
            [15, 27, 39],
            Iter::new([5, 9, 13])
                ->multiply(3)
                ->toValues(),
        );
    }
}

class Double extends AbstractIterator {
    use InnerIteratorTrait;

    public function __construct(iterable|callable $iterable)
    {
        $this->setInnerIterator(new Map($iterable, fn($v) => $v * 2));
    }
}

class Multiply extends AbstractIterator {
    use InnerIteratorTrait;

    public function __construct(iterable|callable $iterable, int|float $factor)
    {
        $this->setInnerIterator(new Map($iterable, fn($v) => $v * $factor));
    }
}
