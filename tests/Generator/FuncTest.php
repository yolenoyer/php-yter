<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Func;
use PHPUnit\Framework\TestCase;

class FuncTest extends TestCase
{
    public function testFunc()
    {
        $n = 3;
        $mul = function() use (&$n) {
            return $n *= 10;
        };

        $this->assertSame([30, 300, 3000], Func::new($mul)->limit(3)->toArray());
    }
}
