<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Forever;
use Dajoha\Iter\Generator\Interleave;
use Iterator;
use PHPUnit\Framework\TestCase;

class InterleaveTest extends TestCase
{
    /**
     * @dataProvider interleaveProvider
     */
    public function testInterleave(
        array|Iterator $outer,
        array|Iterator $inner,
        array $expectedResult,
    ) {
        $it = Interleave::new($outer, $inner)->limit(20);
        $this->assertSame($expectedResult, $it->toValues());
        $it->rewind();
        $this->assertSame($expectedResult, $it->toValues());
    }

    public function interleaveProvider(): array
    {
        return [
            [
                [],
                [],
                [],
            ], [
                [],
                [1],
                [],
            ], [
                [],
                Forever::new(1),
                [],
            ], [
                ['a'],
                [],
                ['a'],
            ], [
                ['a', 'b'],
                [],
                ['a'],
            ], [
                Forever::new('a'),
                [],
                ['a'],
            ], [
                ['a'],
                [1],
                ['a'],
            ], [
                ['a'],
                [1, 2],
                ['a'],
            ], [
                ['a'],
                Forever::new(1),
                ['a'],
            ], [
                ['a', 'b'],
                [1],
                ['a', 1, 'b'],
            ], [
                ['a', 'b'],
                [1, 2],
                ['a', 1, 'b'],
            ], [
                ['a', 'b', 'c'],
                [1, 2],
                ['a', 1, 'b', 2, 'c'],
            ], [
                [1, 2],
                Forever::new(','),
                [1, ',', 2],
            ],
        ];
    }
}
