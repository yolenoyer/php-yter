<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\IteratorInterface;
use PHPUnit\Framework\TestCase;

class CounterTest extends TestCase
{
    /**
     * @dataProvider counterProvider
     */
    public function testCounter(IteratorInterface $iterator, array $expectedValue)
    {
        $this->assertSame($expectedValue, $iterator->toValues());
        $iterator->rewind();
        $this->assertSame($expectedValue, $iterator->toValues());
    }

    public function counterProvider(): array
    {
        return [
            [Counter::to(0), []],
            [Counter::to(0, step: 2), []],
            [Counter::to(6), [0, 1, 2, 3, 4, 5]],
            [Counter::to(6, step: 2), [0, 2, 4]],
            [Counter::to(6, 2), [2, 3, 4, 5]],
            [Counter::from(2)->limit(3), [2, 3, 4]],
            [Counter::from(2, 6), [2, 3, 4, 5]],
            [Counter::sized(4), [0, 1, 2, 3]],
            [Counter::sized(4, step: 2), [0, 2, 4, 6]],
        ];
    }
}
