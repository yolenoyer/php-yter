<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Generator\Zip;
use PHPUnit\Framework\TestCase;

class ZipTest extends TestCase
{
    /**
     * @dataProvider zipProvider
     */
    public function testZip(array $iterators, array $expectedResult, ?int $limit = null)
    {
        $it = Zip::new(...$iterators)->limit($limit);
        $this->assertSame($expectedResult, $it->toArray());
    }

    public function zipProvider(): array
    {
        return [
            [[], []],
            [
                [[], [], []],
                [],
            ], [
                [[1], [2], []],
                [],
            ], [
                [[1], [2], [3]],
                [[1, 2, 3]],
            ], [
                [[1, 7], [2, 8], [3]],
                [[1, 2, 3]],
            ], [
                [[1, 7], [2, 8], [3, 9]],
                [[1, 2, 3], [7, 8, 9]],
            ], [
                [Counter::new(), ['hello', 'world']],
                [[0, 'hello'], [1, 'world']],
            ], [
                Counter::to(4)->map(fn($n) => Counter::new(step: 10 ** $n))->toValues(),
                [
                    [0, 0, 0, 0],
                    [1, 10, 100, 1000],
                    [2, 20, 200, 2000],
                    [3, 30, 300, 3000],
                    [4, 40, 400, 4000],
                ],
                5, // Limit to 5 results
            ],
        ];
    }
}
