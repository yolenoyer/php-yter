<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Dajoha\Iter\Tests;

use Dajoha\Iter\Exception\IterException;
use Dajoha\Iter\Generator\Chars;
use Iterator;
use PHPUnit\Framework\TestCase;

class CharsTest extends TestCase
{
    /**
     * @dataProvider charsProvider
     * @throws IterException
     */
    public function testChars(
        string|Iterator $chars,
        array $expectedArray,
        ?string $expectedFirstChar,
        ?string $expectedLastChar,
        NthCharExpect ...$nthCharExpects,
    ) {
        $it = $chars instanceof Iterator ? $chars : Chars::new($chars);

        $this->assertSame($expectedArray, array_values($it->toValues()));
        $it->rewind();
        $this->assertSame($expectedFirstChar, $it->first());
        $this->assertSame($expectedLastChar, $it->last());

        foreach ($nthCharExpects as $nthCharExpect) {
            $it->rewind();
            $this->assertSame($nthCharExpect->char, $it->nth($nthCharExpect->index));
        }
    }

    /** @noinspection PsalmAdvanceCallableParamsInspection */
    public function charsProvider(): array
    {
        $expect = fn(...$args) => new NthCharExpect(...$args);

        return [
            [
                '',
                [],
                null,
                null,
                $expect(0, null),
                $expect(1, null),
            ], [
                'abc',
                ['a', 'b', 'c'],
                'a',
                'c',
                $expect(0, 'a'),
                $expect(1, 'b'),
                $expect(2, 'c'),
                $expect(3, null),
            ], [
                '(àé)',
                ['(', 'à', 'é', ')'],
                '(',
                ')',
                $expect(0, '('),
                $expect(1, 'à'),
                $expect(2, 'é'),
                $expect(3, ')'),
                $expect(4, null),
            ], [
                Chars::new('abc')
                    ->map(fn($c) => $c === 'b' ? 'z' : $c),
                ['a', 'z', 'c'],
                'a',
                'c',
                $expect(0, 'a'),
                $expect(1, 'z'),
                $expect(2, 'c'),
                $expect(3, null),
            ], [
                Chars::new('aébçcè')
                    ->map(fn($c) => $c === 'b' ? 'z' : $c)
                    ->filter(fn($c) => $c !== 'ç'),
                ['a', 'é', 'z', 'c', 'è'],
                'a',
                'è',
                $expect(0, 'a'),
                $expect(1, 'é'),
                $expect(2, 'z'),
                $expect(3, 'c'),
                $expect(4, 'è'),
                $expect(5, null),
            ],
        ];
    }
}

class NthCharExpect {
    public function __construct(public int $index, public ?string $char)
    {
    }
}
