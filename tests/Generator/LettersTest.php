<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Letters;
use PHPUnit\Framework\TestCase;

class LettersTest extends TestCase
{
    /**
     * @dataProvider lettersProvider
     */
    public function testLetters(array $sequences, array $expectedResult)
    {
        $it = Letters::new(...$sequences);
        $this->assertSame($expectedResult, $it->toValues());
    }

    public function lettersProvider(): array
    {
        return [
            [
                [Letters::DIGITS],
                ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
            ], [
                [Letters::HEX_DIGITS],
                ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'],
            ], [
                [Letters::HEX_DIGITS_UPPERCASE],
                ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'],
            ], [
                [Letters::HEX_DIGITS_LOWERCASE],
                ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'],
            ], [
                [Letters::LETTERS_LOWERCASE],
                [
                    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                ],
            ], [
                [Letters::LETTERS_UPPERCASE],
                [
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                ],
            ], [
                [Letters::LETTERS_LOWERCASE, Letters::LETTERS_UPPERCASE],
                [
                    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                ],
            ],
        ];
    }

    /**
     * @dataProvider lettersWithUnionProvider
     */
    public function testLettersWithUnions(int $sequence, array $expectedChars)
    {
        $array = Letters::new($sequence)->toValues();
        sort($array);
        sort($expectedChars);
        $this->assertSame($expectedChars, $array);
    }

    public function lettersWithUnionProvider(): array
    {
        return [
            [
                Letters::DIGITS | Letters::LETTERS_LOWERCASE,
                [
                    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                ],
            ], [
                Letters::LETTERS_LOWERCASE | Letters::LETTERS_UPPERCASE,
                [
                    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                ],
            ],
        ];
    }
}
