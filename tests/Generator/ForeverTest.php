<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Forever;
use PHPUnit\Framework\TestCase;

class ForeverTest extends TestCase
{
    public function testForever()
    {
        $it = Forever::new(3);

        for ($i = 0; $i < 20; ++$i) {
            $this->assertSame(3, $it->current());
            $it->next();
        }

        $this->assertSame([1, 1], Forever::new(1)->limit(2)->toValues());
    }
}
