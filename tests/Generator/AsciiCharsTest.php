<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests;

use Dajoha\Iter\Exception\IterException;
use Dajoha\Iter\Generator\AsciiChars;
use Iterator;
use PHPUnit\Framework\TestCase;

class AsciiCharsTest extends TestCase
{
    /**
     * @dataProvider asciiCharsProvider
     * @throws IterException
     */
    public function testAsciiChars(
        string|Iterator $chars,
        array $expectedArray,
        ?string $expectedFirstChar,
        ?string $expectedLastChar,
        NthAsciiCharExpect ...$nthAsciiCharExpects,
    ) {
        $it = $chars instanceof Iterator ? $chars : AsciiChars::new($chars);

        $this->assertSame($expectedArray, $it->toValues());
        $it->rewind();
        $this->assertSame($expectedFirstChar, $it->first());
        $this->assertSame($expectedLastChar, $it->last());

        foreach ($nthAsciiCharExpects as $nthAsciiCharExpect) {
            $it->rewind();
            $this->assertSame($nthAsciiCharExpect->char, $it->nth($nthAsciiCharExpect->index));
        }
    }

    /** @noinspection PsalmAdvanceCallableParamsInspection */
    public function asciiCharsProvider(): array
    {
        $expect = fn(...$args) => new NthAsciiCharExpect(...$args);

        return [
            [
                '',
                [],
                null,
                null,
                $expect(0, null),
                $expect(1, null),
            ], [
                'abc',
                ['a', 'b', 'c'],
                'a',
                'c',
                $expect(0, 'a'),
                $expect(1, 'b'),
                $expect(2, 'c'),
                $expect(3, null),
            ], [
                '(àé)',
                ['(', "\xc3", "\xa0", "\xc3", "\xa9", ')'],
                '(',
                ')',
                $expect(0, '('),
                $expect(1, "\xc3"),
                $expect(2, "\xa0"),
                $expect(3, "\xc3"),
                $expect(4, "\xa9"),
                $expect(5, ')'),
                $expect(6, null),
            ], [
                AsciiChars::new('abc')
                    ->map(fn($c) => $c === 'b' ? 'z' : $c),
                ['a', 'z', 'c'],
                'a',
                'c',
                $expect(0, 'a'),
                $expect(1, 'z'),
                $expect(2, 'c'),
                $expect(3, null),
            ],
        ];
    }
}

class NthAsciiCharExpect {
    public function __construct(public int $index, public ?string $char)
    {
    }
}
