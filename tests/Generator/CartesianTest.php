<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Cartesian;
use Iterator;
use PHPUnit\Framework\TestCase;

class CartesianTest extends TestCase
{
    /**
     * @dataProvider cartesianProvider
     */
    public function testCartesian(array $iterators, array $expected)
    {
        $result = Cartesian::new(...$iterators)->toArray();

        $this->assertSame($expected, $result);
    }

    /**
     * @return array{iterators: array|Iterator, expected: array}[]
     */
    public function cartesianProvider(): array
    {
        return [
            [
                'iterators' => [[]],
                'expected' => [],
            ], [
                'iterators' => [[], []],
                'expected' => [],
            ], [
                'iterators' => [[], [], []],
                'expected' => [],
            ], [
                'iterators' => [[1], [], []],
                'expected' => [],
            ], [
                'iterators' => [[], [1], []],
                'expected' => [],
            ], [
                'iterators' => [[], [], [1]],
                'expected' => [],
            ], [
                'iterators' => [[1], [2], []],
                'expected' => [],
            ], [
                'iterators' => [[1], [2], [3]],
                'expected' => [[1, 2, 3]],
            ], [
                'iterators' => [[1], [2, 3]],
                'expected' => [[1, 2], [1, 3]],
            ], [
                'iterators' => [[1, 2], [3]],
                'expected' => [[1, 3], [2, 3]],
            ], [
                'iterators' => [[1, 2], [3, 4]],
                'expected' => [[1, 3], [1, 4], [2, 3], [2, 4]],
            ], [
                'iterators' => [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                'expected' => [
                    [1, 4, 7],
                    [1, 4, 8],
                    [1, 4, 9],
                    [1, 5, 7],
                    [1, 5, 8],
                    [1, 5, 9],
                    [1, 6, 7],
                    [1, 6, 8],
                    [1, 6, 9],
                    [2, 4, 7],
                    [2, 4, 8],
                    [2, 4, 9],
                    [2, 5, 7],
                    [2, 5, 8],
                    [2, 5, 9],
                    [2, 6, 7],
                    [2, 6, 8],
                    [2, 6, 9],
                    [3, 4, 7],
                    [3, 4, 8],
                    [3, 4, 9],
                    [3, 5, 7],
                    [3, 5, 8],
                    [3, 5, 9],
                    [3, 6, 7],
                    [3, 6, 8],
                    [3, 6, 9],
                ],
            ],
        ];
    }
}
