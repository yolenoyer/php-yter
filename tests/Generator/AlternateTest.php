<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Counter;
use Dajoha\Iter\Generator\Alternate;
use Dajoha\Iter\Generator\Letters;
use PHPUnit\Framework\TestCase;

class AlternateTest extends TestCase
{
    /**
     * @dataProvider alternateProvider
     */
    public function testAlternate(array $iterators, array $expectedResult, ?int $limit = null)
    {
        $it = Alternate::new(...$iterators)->limit($limit);
        $this->assertSame($expectedResult, $it->toValues());
        $it->rewind();
        $this->assertSame($expectedResult, $it->toValues());
    }

    public function alternateProvider(): array
    {
        return [
            [
                [],
                [],
            ], [
                [[], [], []],
                [],
            ], [
                [['A']],
                ['A'],
            ], [
                [['A'], []],
                ['A'],
            ], [
                [[], ['A']],
                ['A'],
            ], [
                [['A'], [], []],
                ['A'],
            ], [
                [[], [], [], ['A'], [], []],
                ['A'],
            ], [
                [['A1', 'A2']],
                ['A1', 'A2'],
            ], [
                [['A1', 'A2', 'A3', 'A4']],
                ['A1', 'A2', 'A3', 'A4'],
            ], [
                [['A'], ['B']],
                ['A', 'B'],
            ], [
                [['A'], ['B'], []],
                ['A', 'B'],
            ], [
                [['A'], ['B'], ['C']],
                ['A', 'B', 'C'],
            ], [
                [['A1', 'A2'], []],
                ['A1', 'A2'],
            ], [
                [['A1', 'A2'], ['B1']],
                ['A1', 'B1', 'A2'],
            ], [
                [['A1'], ['B1', 'B2']],
                ['A1', 'B1', 'B2'],
            ], [
                [['A1', 'A2'], ['B1', 'B2']],
                ['A1', 'B1', 'A2', 'B2'],
            ], [
                [['A1', 'A2', 'A3'], ['B1', 'B2']],
                ['A1', 'B1', 'A2', 'B2', 'A3'],
            ], [
                [['A1', 'A2'], ['B1', 'B2', 'B3']],
                ['A1', 'B1', 'A2', 'B2', 'B3'],
            ], [
                [['A1', 'A2'], ['B1', 'B2', 'B3', 'B4', 'B5']],
                ['A1', 'B1', 'A2', 'B2', 'B3', 'B4', 'B5'],
            ], [
                [['A1', 'A2'], [], ['B1', 'B2', 'B3']],
                ['A1', 'B1', 'A2', 'B2', 'B3'],
            ], [
                [['A1', 'A2'], ['B1', 'B2'], ['C1', 'C2']],
                ['A1', 'B1', 'C1', 'A2', 'B2', 'C2'],
            ], [
                [['A1', 'A2'], ['B1', 'B2', 'B3', 'B4'], ['C1', 'C2', 'C3']],
                ['A1', 'B1', 'C1', 'A2', 'B2', 'C2', 'B3', 'C3', 'B4'],
            ], [
                [[], ['A1', 'A2'], [], ['B1', 'B2', 'B3', 'B4'], [], [], ['C1', 'C2', 'C3']],
                ['A1', 'B1', 'C1', 'A2', 'B2', 'C2', 'B3', 'C3', 'B4'],
            ], [
                [[], ['A1', 'A2'], [], ['B1', 'B2', 'B3', 'B4'], [], [], ['C1', 'C2', 'C3'], ['D1']],
                ['A1', 'B1', 'C1', 'D1', 'A2', 'B2', 'C2', 'B3', 'C3', 'B4'],
            ], [
                [
                    Letters::new(Letters::LETTERS_UPPERCASE)->filter(fn($c) => $c !== 'C'),
                    Counter::from(10, step: 10),
                ],
                ['A', 10, 'B', 20, 'D', 30, 'E', 40, 'F'],
                9,
            ],
        ];
    }
}
