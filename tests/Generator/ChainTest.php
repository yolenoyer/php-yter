<?php

declare(strict_types=1);

namespace Dajoha\Iter\Tests\Generator;

use Dajoha\Iter\Generator\Chain;
use Dajoha\Iter\Generator\Counter;
use PHPUnit\Framework\TestCase;

class ChainTest extends TestCase
{
    public function testChain()
    {
        $it = Chain::new([1, 2, 3], [4, 5, 6]);

        $this->assertSame($it->toValues(), [1, 2, 3, 4, 5, 6]);
        $it->rewind();
        $it = $it->skip(3);
        $this->assertSame(4, $it->find(fn($n) => $n % 2 === 0));
        $this->assertSame(6, $it->find(fn($n) => $n % 2 === 0));
        $this->assertSame(null, $it->find(fn($n) => $n % 2 === 0));
    }

    public function testEmptyChain()
    {
        $it = Chain::new();
        $this->assertSame($it->toValues(), []);

        $it = Chain::new([], [], []);
        $this->assertSame($it->toValues(), []);
    }

    public function testComplexChain()
    {
        $it = Counter::new(6)->map(fn($n) => Counter::new($n));
        $it = Chain::new(...$it->toValues());

        $this->assertSame([0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4], $it->toArray());

        $it->rewind();
        $this->assertSame([0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4], $it->toArray());

        $it->rewind();
        $it = $it->filter(fn($n) => $n !== 0);
        $this->assertSame([1, 1, 2, 1, 2, 3, 1, 2, 3, 4], $it->toValues());
    }

    public function testChainWithOriginalKeys()
    {
        $iterables = [
            [
                'foo' => 3,
                'bar' => 5,
            ],
            Counter::new(3),
            iter(['one', 'two', 'three'])->mapKeys(fn($k, $v) => $v),
        ];
        $chain = Chain::new(...$iterables)->withOriginalKeys();

        $this->assertSame([
            'foo' => 3,
            'bar' => 5,
            0,
            1,
            2,
            'one' => 'one',
            'two' => 'two',
            'three' => 'three',
        ], $chain->toArray());
    }
}
