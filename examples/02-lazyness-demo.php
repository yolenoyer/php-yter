<?php

/**
 * This example demonstrates that iterators are not consumed when they are created.
 */

declare(strict_types=1);

include_once __DIR__.'/../vendor/autoload.php';

$numbers = [-10, -78, 110, -3, 42, 2700];

$it = iter($numbers)
    ->filter(function ($n) {
        $filtered = $n > 0;
        if ($filtered) {
            echo "[filter] Keeping $n:)\n";
        } else {
            echo "[filter] Skipping $n:/\n";
        }
        return $filtered;
    })
    ->map(function ($n) {
        echo "[map] Mapping $n...\n";
        sleep(2); // Sleeping two seconds in order to simulate a huge process
        return $n * 10;
    })
;

echo "The iterator has been created (this message should be displayed immediately).\n\n";

echo "So, let's consume the iterator with a \"foreach\" loop...\n\n";
foreach ($it as $number) {
    echo ">> Value $number was processed!\n";
}

echo "\n";
echo "Ok, then let's rewind the iterator, and consume it with the `toValues()` method...\n\n";
$it->rewind();
var_dump($it->toValues());
