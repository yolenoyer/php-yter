<?php

/**
 * This example shows how to search for elements.
 */

declare(strict_types=1);

include_once __DIR__.'/../vendor/autoload.php';

$numbers = [45, 700, 12, 0, 17, -178, 20];
$numbers = iter($numbers);

$hasDigit7 = fn(int $n) => str_contains((string) $n, '7');

// `isFound()` returns `true` when something is found, and set the 2nd parameter to the found value:
echo "Using `isFound()`:\n";
while ($numbers->isFound($hasDigit7, $n)) {
    echo " -> Number $n has a 7 digit\n";
}

$numbers->rewind();
echo "\n";

// `find()` returns `null` when not found (then it may be useless if `null` is an expected value):
echo "Using `find()`:\n";
while (null !== ($n = $numbers->find($hasDigit7))) {
    echo " -> Number $n has a 7 digit\n";
}
