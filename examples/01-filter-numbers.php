<?php

/**
 * A basic usage example.
 */

declare(strict_types=1);

include_once __DIR__.'/../vendor/autoload.php';

$numbers = [-10, -78, 110, -3, 42, 2700];

$positiveNumbers = iter($numbers)
    ->filter(fn($n) => $n > 0)
    ->toValues();

var_dump($positiveNumbers);
