<?php

/**
 * This example shows how to use the `Zip` iterator.
 */

declare(strict_types=1);

use Dajoha\Iter\Generator\Zip;

include_once __DIR__.'/../vendor/autoload.php';

$persons = [
    'Yoda',
    'Chewbacca',
    'Luke',
    'Anakin',
];
$sentences = [
    "The force is with you",
    "Grmmph",
    "Noooo!!!!",
    "I am your father",
    "Have a bad feeling about it",
    "I know.",
];
$secretNumberGenerator = fn() => rand(0, 100); // Infinite generator

$zip = new Zip($persons, $secretNumberGenerator, $sentences);
// This could as well have been defined like this:
// $zip = iter($persons)->zip($secretNumberGenerator, $sentences);

foreach ($zip as [$person, $secretNumber, $sentence]) {
    echo "The secret number for $person is $secretNumber! How do you feel about it? -- $sentence\n";
}
