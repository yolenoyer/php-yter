<?php

declare(strict_types=1);

use Util\Bench;

include_once __DIR__.'/../../vendor/autoload.php';
include_once __DIR__.'/Util/Bench.php';

$numbers = [45, 1, 8, 98, 2, 31, 58, 53];

$nativeBench = function() use ($numbers) {
    $bench = new Bench("Few numbers filtered and multiplied (native php)");
    $bench->start();

    $result = [];
    foreach ($numbers as $number) {
        if ($number <= 50) {
            continue;
        }
        $result[] = $number * 10;
    }

    $bench->stopAndShowResult();
};

$phpIterBench = function() use ($numbers) {
    $bench = new Bench("Few numbers filtered and multiplied (php-iter)");
    $bench->start();

    $result = iter($numbers)
        ->filter(fn($n) => $n > 50)
        ->map(fn($n) => $n * 10)
        ->toValues();

    $bench->stopAndShowResult();
};

$nativeBench();
$phpIterBench();
