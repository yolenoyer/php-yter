<?php

declare(strict_types=1);

use Util\Bench;

include_once __DIR__.'/../../vendor/autoload.php';
include_once __DIR__.'/Util/Bench.php';

$testSize = 100_000;

$numbers = [];
for ($i = 0; $i < $testSize; $i++) {
    $numbers[] = random_int(0, 100);
}

$nativeBenchArrayMap = function() use ($testSize, $numbers) {
    $bench = new Bench("$testSize numbers multiplied (native php with array_map())");
    $bench->start();

    $result = array_map(fn($n) => $n * 10, $numbers);

    $bench->stopAndShowResult();
};

$nativeBenchForLoop = function() use ($testSize, $numbers) {
    $bench = new Bench("$testSize numbers multiplied (native php with for loop)");
    $bench->start();

    $result = [];
    foreach ($numbers as $number) {
        $result[] = $number * 10;
    }

    $bench->stopAndShowResult();
};

$phpIterBench = function() use ($testSize, $numbers) {
    $bench = new Bench("$testSize numbers multiplied (php-iter)");
    $bench->start();

    $result = iter($numbers)
        ->map(fn($n) => $n * 10)
        ->toValues();

    $bench->stopAndShowResult();
};

$nativeBenchArrayMap();
$nativeBenchForLoop();
$phpIterBench();
