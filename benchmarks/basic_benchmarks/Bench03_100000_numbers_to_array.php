<?php

declare(strict_types=1);

use Util\Bench;

include_once __DIR__.'/../../vendor/autoload.php';
include_once __DIR__.'/Util/Bench.php';

$numbers = [];
for ($i = 0; $i < 100_000; $i++) {
    $numbers[] = random_int(0, 100);
}

$nativeBenchForLoop = function() use ($numbers) {
    $bench = new Bench("Few numbers just converted to array (native php with for loop)");
    $bench->start();

    $result = [];
    foreach ($numbers as $number) {
        $result[] = $number;
    }

    $bench->stopAndShowResult();
};

$phpIterBench = function() use ($numbers) {
    $bench = new Bench("Few numbers just converted to array (php-iter)");
    $bench->start();

    $result = iter($numbers)->toValues();

    $bench->stopAndShowResult();
};

$nativeBenchForLoop();
$phpIterBench();
