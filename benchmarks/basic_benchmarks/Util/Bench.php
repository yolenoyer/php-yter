<?php

declare(strict_types=1);

namespace Util;

class Bench
{
    protected float $startTime = 0;

    protected float $stopTime = 0;

    public function __construct(protected ?string $title = null)
    {
    }

    public function start(): void
    {
        $this->startTime = microtime(true);
    }

    public function stop(): void
    {
        $this->stopTime = microtime(true);
    }

    public function showResult(): void
    {
        $duration = $this->getDuration();

        if ($this->title !== null) {
            echo sprintf("Elapsed time for \"%s\": %s s\n", $this->title, $duration);
        } else {
            echo sprintf("Elapsed time: %s s\n", $duration);
        }
    }

    public function getDuration(): float
    {
        return $this->stopTime - $this->startTime;
    }

    public function stopAndShowResult(): void
    {
        $this->stop();
        $this->showResult();
    }
}
