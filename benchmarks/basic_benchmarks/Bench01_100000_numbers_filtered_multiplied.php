<?php

declare(strict_types=1);

use Util\Bench;

include_once __DIR__.'/../../vendor/autoload.php';
include_once __DIR__.'/Util/Bench.php';

$testSize = 100_000;

$numbers = [];
for ($i = 0; $i < $testSize; $i++) {
    $numbers[] = random_int(0, 100);
}

$nativeBench = function() use ($testSize, $numbers) {
    $bench = new Bench("$testSize numbers filtered and multiplied (native php)");
    $bench->start();

    $result = [];
    foreach ($numbers as $number) {
        if ($number <= 50) {
            continue;
        }
        $result[] = $number * 10;
    }

    $bench->stopAndShowResult();
};

$phpIterBench = function() use ($testSize, $numbers) {
    $bench = new Bench("$testSize numbers filtered and multiplied (php-iter)");
    $bench->start();

    $result = iter($numbers)
        ->filter(fn($n) => $n > 50)
        ->map(fn($n) => $n * 10)
        ->toValues();

    $bench->stopAndShowResult();
};

$nativeBench();
$phpIterBench();
