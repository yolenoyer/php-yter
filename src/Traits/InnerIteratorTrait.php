<?php

declare(strict_types=1);

namespace Dajoha\Iter\Traits;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Implements a basic iterator, from an inner iterator.
 *
 * @template TKey
 * @template TValue
 */
trait InnerIteratorTrait
{
    /** @var Iterator<TKey, TValue> */
    protected Iterator $innerIterator;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $innerIterator
     */
    protected function setInnerIterator(iterable|callable $innerIterator): void
    {
        $this->innerIterator = AbstractIterator::toIterator($innerIterator);
    }

    public function current(): mixed
    {
        return $this->innerIterator->current();
    }

    public function next(): void
    {
        $this->innerIterator->next();
    }

    public function key(): mixed
    {
        return $this->innerIterator->key();
    }

    public function valid(): bool
    {
        return $this->innerIterator->valid();
    }

    public function rewind(): void
    {
        $this->innerIterator->rewind();
    }
}
