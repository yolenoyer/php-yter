<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Skip the first *n* iterations of the child iterator.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Skip extends AbstractIterator
{
    protected Iterator $iterator;

    protected bool $currentIsUpToDate = false;

    protected int $currentSkip;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     */
    public function __construct(iterable|callable $iterable, protected int $skip)
    {
        $this->iterator = self::toIterator($iterable);
        $this->currentSkip = $this->skip;
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable, int $limit): self
    {
        return new self($iterable, $limit);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;

        while ($this->iterator->valid()) {
            if ($this->currentSkip === 0) {
                break;
            }
            --$this->currentSkip;
            $this->iterator->next();
        }

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->iterator->current();
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        $this->iterator->next();
    }

    public function key(): mixed
    {
        $this->updateIterator();

        return $this->iterator->key();
    }

    public function valid(): bool
    {
        $this->updateIterator();

        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->currentSkip = $this->skip;
        $this->iterator->rewind();
    }
}
