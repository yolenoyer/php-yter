<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Loop the child iterator *n* times, or indefinitely.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Loop extends AbstractIterator
{
    protected Iterator $iterator;

    protected int $currentLoop = 0;

    protected int $currentKey = 0;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     */
    public function __construct(iterable|callable $iterable, protected ?int $count = null)
    {
        $this->iterator = self::toIterator($iterable);
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable, ?int $count = null): self
    {
        return new self($iterable, $count);
    }

    public function current(): mixed
    {
        return $this->iterator->current();
    }

    public function next(): void
    {
        $this->iterator->next();
        ++$this->currentKey;
    }

    public function key(): int
    {
        return $this->currentKey;
    }

    public function valid(): bool
    {
        if (!$this->iterator->valid()) {
            $this->iterator->rewind();
            ++$this->currentLoop;
        }

        return
            ($this->count === null || $this->currentLoop < $this->count)
            && $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->iterator->rewind();
        $this->currentLoop = 0;
        $this->currentKey = 0;
    }
}
