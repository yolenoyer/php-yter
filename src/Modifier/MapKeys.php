<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Map the keys of the child iterator.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class MapKeys extends AbstractIterator
{
    protected Iterator $iterator;

    /** @var callable */
    protected $keyMapper;

    protected bool $currentIsUpToDate = false;

    protected mixed $currentKey;

    protected int $currentKeyIndex = 0;

    /**
     * @template K
     *
     * @phpstan-param iterable<K, TValue>|(callable(): TValue) $iterable
     * @phpstan-param callable(K $key, TValue $value, int $index): TKey $keyMapper
     */
    public function __construct(iterable|callable $iterable, callable $keyMapper)
    {
        $this->iterator = self::toIterator($iterable);
        $this->keyMapper = $keyMapper;
    }

    /**
     * @template K1
     * @template K2
     * @template V
     *
     * @phpstan-param iterable<K1, V>|(callable(): V) $iterable
     * @phpstan-param callable(K1 $key, V $value, int $index): K2 $keyMapper
     *
     * @return self<K2, V>
     */
    public static function new(iterable|callable $iterable, callable $keyMapper): self
    {
        return new self($iterable, $keyMapper);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;

        $this->currentKey = ($this->keyMapper)(
            $this->iterator->key(),
            $this->iterator->current(),
            $this->currentKeyIndex,
        );

        return true;
    }

    public function current(): mixed
    {
        return $this->iterator->current();
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        ++$this->currentKeyIndex;
        $this->iterator->next();
    }

    public function key(): mixed
    {
        $this->updateIterator();

        return $this->currentKey;
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->currentKeyIndex = 0;
        $this->iterator->rewind();
    }
}
