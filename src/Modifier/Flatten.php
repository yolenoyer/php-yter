<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Flatten a (main) iterator of (sub) iterables. Each element of the main iterator must be
 * iterable.
 *
 * @extends AbstractIterator<mixed, mixed>
 */
class Flatten extends AbstractIterator
{
    protected Iterator $iterator;

    protected ?Iterator $subIterator = null;

    protected bool $subIteratorIsUpToDate = false;

    /**
     * @template V of iterable<mixed>|callable
     *
     * @phpstan-param iterable<mixed, V>|(callable(): V) $iterable
     */
    public function __construct(iterable|callable $iterable)
    {
        $this->iterator = self::toIterator($iterable);
    }

    /**
     * @template V of iterable<mixed>|callable
     *
     * @phpstan-param iterable<mixed, V>|(callable(): V) $iterable
     */
    public static function new(iterable|callable $iterable): self
    {
        return new self($iterable);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateSubIterator(): bool
    {
        if ($this->subIteratorIsUpToDate) {
            return false;
        }
        $this->subIteratorIsUpToDate = true;
        // @phpstan-ignore-next-line
        $this->subIterator = self::toIterator($this->iterator->current());

        return true;
    }

    public function current(): mixed
    {
        $this->updateSubIterator();

        return $this->subIterator->current();
    }

    public function next(): void
    {
        $this->updateSubIterator();
        $this->subIterator->next();
        if (!$this->subIterator->valid()) {
            $this->subIteratorIsUpToDate = false;
            $this->iterator->next();
        }
    }

    public function key(): mixed
    {
        $this->updateSubIterator();

        return $this->subIterator->key();
    }

    public function valid(): bool
    {
        while (true) {
            if (!$this->iterator->valid()) {
                return false;
            }
            $this->updateSubIterator();
            if ($this->subIterator->valid()) {
                return true;
            } else {
                $this->subIteratorIsUpToDate = false;
                $this->iterator->next();
            }
        }
    }

    public function rewind(): void
    {
        $this->iterator->rewind();
        $this->subIteratorIsUpToDate = false;
    }
}
