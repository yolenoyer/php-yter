<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Dajoha\Iter\Exception\IterException;
use Iterator;

/**
 * Split the child iterator items into chunks of <N> elements, except the last one which can be
 * smaller. Keys are not preserved.
 *
 * @template TValue
 *
 * @extends AbstractIterator<int, TValue[]>
 */
class Chunks extends AbstractIterator
{
    protected Iterator $iterator;

    protected bool $currentIsUpToDate = false;

    /** @var TValue[] */
    protected array $currentChunk = [];

    protected int $currentKeyIndex = 0;

    /**
     * @phpstan-param iterable<mixed, TValue>|(callable(): TValue) $iterable
     *
     * @throws IterException
     */
    public function __construct(
        iterable|callable $iterable,
        protected int $size,
    ) {
        if ($size <= 0) {
            throw new IterException("Negative or zero sizes are forbidden: $size");
        }

        $this->iterator = self::toIterator($iterable);
    }

    /**
     * @throws IterException
     *
     * @template V
     *
     * @phpstan-param iterable<mixed, V>|(callable(): V) $iterable
     *
     * @return self<V>
     */
    public static function new(iterable|callable $iterable, int $size): self
    {
        return new self($iterable, $size);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;
        $this->currentChunk = [];

        for ($i = 0; $i < $this->size; ++$i) {
            if (!$this->iterator->valid()) {
                break;
            }
            $this->currentChunk[] = $this->iterator->current();
            $this->iterator->next();
        }

        return true;
    }

    /**
     * @return TValue[]
     */
    public function current(): array
    {
        $this->updateIterator();

        return $this->currentChunk;
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        ++$this->currentKeyIndex;
    }

    public function key(): int
    {
        $this->updateIterator();

        return $this->currentKeyIndex;
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->currentChunk = [];
        $this->currentKeyIndex = 0;
        $this->iterator->rewind();
    }
}
