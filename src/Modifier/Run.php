<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Dajoha\Iter\Traits\InnerIteratorTrait;

/**
 * Don't change the behaviour of the child iterator, but execute the given callable on each
 * iteration.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Run extends AbstractIterator
{
    /** @use InnerIteratorTrait<TKey, TValue> */
    use InnerIteratorTrait;

    /** @var (callable(TValue $value): mixed)|null */
    protected $callable;

    protected bool $currentIsUpToDate = false;

    protected mixed $currentValue;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     * @phpstan-param (callable(TValue $value): mixed)|null $callable The return value is not used
     */
    public function __construct(iterable|callable $iterable, ?callable $callable)
    {
        $this->setInnerIterator($iterable);
        $this->callable = $callable;
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     * @phpstan-param (callable(TValue $value): mixed)|null $callable The return value is not used
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable, ?callable $callable): self
    {
        return new self($iterable, $callable);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;

        $this->currentValue = $this->innerIterator->current();
        if (is_callable($this->callable)) {
            ($this->callable)($this->currentValue);
        }

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->currentValue;
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        $this->innerIterator->next();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->innerIterator->rewind();
    }
}
