<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Map the values of the child iterator.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Map extends AbstractIterator
{
    protected Iterator $iterator;

    /** @var callable */
    protected $mapper;

    protected bool $currentIsUpToDate = false;

    protected mixed $currentValue;

    protected int $currentKeyIndex = 0;

    /**
     * @template V
     *
     * @phpstan-param iterable<TKey, V>|(callable(): V) $iterable
     * @phpstan-param callable(V $value, TKey $key, int $index): TValue $mapper
     */
    public function __construct(iterable|callable $iterable, callable $mapper)
    {
        $this->iterator = self::toIterator($iterable);
        $this->mapper = $mapper;
    }

    /**
     * @template K
     * @template V1
     * @template V2
     *
     * @phpstan-param iterable<K, V1>|(callable(): V1) $iterable
     * @phpstan-param callable(V1 $value, K $key, int $index): V2 $mapper
     *
     * @return self<K, V2>
     */
    public static function new(iterable|callable $iterable, callable $mapper): self
    {
        return new self($iterable, $mapper);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;

        $this->currentValue = ($this->mapper)(
            $this->iterator->current(),
            $this->iterator->key(),
            $this->currentKeyIndex,
        );

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->currentValue;
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        ++$this->currentKeyIndex;
        $this->iterator->next();
    }

    public function key(): mixed
    {
        return $this->iterator->key();
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->currentKeyIndex = 0;
        $this->iterator->rewind();
    }
}
