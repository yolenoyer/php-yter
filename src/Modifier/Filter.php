<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Filter the child iterator by checking each value with a predicate.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Filter extends AbstractIterator
{
    protected Iterator $iterator;

    /** @var callable */
    protected $filter;

    protected bool $currentIsUpToDate = false;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     * @phpstan-param callable(TValue $value, TKey $key): bool $filter
     */
    public function __construct(iterable|callable $iterable, callable $filter)
    {
        $this->iterator = self::toIterator($iterable);
        $this->filter = $filter;
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     * @phpstan-param callable(V $value, K $key): bool $filter
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable, callable $filter): self
    {
        return new self($iterable, $filter);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;

        while ($this->iterator->valid()) {
            if (($this->filter)($this->iterator->current(), $this->iterator->key())) {
                break;
            }
            $this->iterator->next();
        }

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->iterator->current();
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        $this->iterator->next();
    }

    public function key(): mixed
    {
        $this->updateIterator();

        return $this->iterator->key();
    }

    public function valid(): bool
    {
        $this->updateIterator();

        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->iterator->rewind();
    }
}
