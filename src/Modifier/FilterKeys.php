<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Filter the child iterator by checking each key with a predicate.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class FilterKeys extends AbstractIterator
{
    protected Iterator $iterator;

    /** @var callable */
    protected $keyFilter;

    protected bool $currentIsUpToDate = false;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     * @phpstan-param callable(TKey $key, TValue $value): bool $keyFilter
     */
    public function __construct(iterable|callable $iterable, callable $keyFilter)
    {
        $this->iterator = self::toIterator($iterable);
        $this->keyFilter = $keyFilter;
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     * @phpstan-param callable(K $key, V $value): bool $keyFilter
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable, callable $keyFilter): self
    {
        return new self($iterable, $keyFilter);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;

        while ($this->iterator->valid()) {
            if (($this->keyFilter)($this->iterator->key(), $this->iterator->current())) {
                break;
            }
            $this->iterator->next();
        }

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->iterator->current();
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        $this->iterator->next();
    }

    public function key(): mixed
    {
        $this->updateIterator();

        return $this->iterator->key();
    }

    public function valid(): bool
    {
        $this->updateIterator();

        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->iterator->rewind();
    }
}
