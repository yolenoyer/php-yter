<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Limit the child iterator to the given maximum iterations.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Limit extends AbstractIterator
{
    protected Iterator $iterator;

    protected ?int $currentLimit;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     */
    public function __construct(iterable|callable $iterable, protected ?int $limit = null)
    {
        $this->iterator = self::toIterator($iterable);
        $this->currentLimit = $this->limit;
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable, ?int $limit = null): self
    {
        return new self($iterable, $limit);
    }

    public function current(): mixed
    {
        return $this->currentLimit === 0 ? null : $this->iterator->current();
    }

    public function next(): void
    {
        if ($this->currentLimit !== 0) {
            $this->iterator->next();
            if ($this->currentLimit !== null) {
                --$this->currentLimit;
            }
        }
    }

    public function key(): mixed
    {
        return $this->currentLimit === 0 ? null : $this->iterator->key();
    }

    public function valid(): bool
    {
        return $this->currentLimit !== 0 && $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentLimit = $this->limit;
        $this->iterator->rewind();
    }
}
