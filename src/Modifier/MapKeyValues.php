<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Map the keys and values of the child iterator.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class MapKeyValues extends AbstractIterator
{
    protected Iterator $iterator;

    /** @var callable */
    protected $keyValueMapper;

    protected bool $currentIsUpToDate = false;

    protected mixed $currentKey;
    protected mixed $currentValue;

    protected int $currentKeyIndex = 0;

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     * @phpstan-param callable(K $key, V $value, int $index): list{TKey, TValue} $keyValueMapper
     */
    public function __construct(iterable|callable $iterable, callable $keyValueMapper)
    {
        $this->iterator = self::toIterator($iterable);
        $this->keyValueMapper = $keyValueMapper;
    }

    /**
     * @template K1
     * @template K2
     * @template V1
     * @template V2
     *
     * @phpstan-param iterable<K1, V1>|(callable(): V1) $iterable
     * @phpstan-param callable(K1 $key, V1 $value, int $index): list{K2, V2} $keyValueMapper
     *
     * @return self<K2, V2>
     */
    public static function new(iterable|callable $iterable, callable $keyValueMapper): self
    {
        return new self($iterable, $keyValueMapper);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->currentIsUpToDate) {
            return false;
        }
        $this->currentIsUpToDate = true;

        [$this->currentKey, $this->currentValue] = ($this->keyValueMapper)(
            $this->iterator->key(),
            $this->iterator->current(),
            $this->currentKeyIndex,
        );

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->currentValue;
    }

    public function next(): void
    {
        $this->currentIsUpToDate = false;
        ++$this->currentKeyIndex;
        $this->iterator->next();
    }

    public function key(): mixed
    {
        $this->updateIterator();

        return $this->currentKey;
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->currentIsUpToDate = false;
        $this->currentKeyIndex = 0;
        $this->iterator->rewind();
    }
}
