<?php

declare(strict_types=1);

namespace Dajoha\Iter\Modifier;

use Dajoha\Iter\Traits\InnerIteratorTrait;
use Dajoha\Iter\AbstractIterator;

/**
 * Both skip and limit the child iterator.
 *
 * @see Skip
 * @see Limit
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Slice extends AbstractIterator
{
    /** @use InnerIteratorTrait<TKey, TValue> */
    use InnerIteratorTrait;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     */
    public function __construct(iterable|callable $iterable, int $start = 0, ?int $length = null)
    {
        $this->setInnerIterator(Skip::new($iterable, $start)->limit($length));
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable, int $start = 0, ?int $length = null): self
    {
        return new self($iterable, $start, $length);
    }
}
