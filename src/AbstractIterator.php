<?php

declare(strict_types=1);

namespace Dajoha\Iter;

use ArrayIterator;
use Dajoha\Iter\Exception\IterException;
use Dajoha\Iter\Generator\Alternate;
use Dajoha\Iter\Generator\Cartesian;
use Dajoha\Iter\Generator\Chain;
use Dajoha\Iter\Generator\Forever;
use Dajoha\Iter\Generator\Func;
use Dajoha\Iter\Generator\Interleave;
use Dajoha\Iter\Modifier\Chunks;
use Dajoha\Iter\Modifier\FilterKeys;
use Dajoha\Iter\Modifier\Flatten;
use Dajoha\Iter\Modifier\MapKeys;
use Dajoha\Iter\Modifier\MapKeyValues;
use Dajoha\Iter\Modifier\Run;
use Dajoha\Iter\Traits\InnerIteratorTrait;
use Iterator;
use IteratorIterator;
use LogicException;
use RuntimeException;
use Dajoha\Iter\Generator\Zip;
use Dajoha\Iter\Modifier\Filter;
use Dajoha\Iter\Modifier\Limit;
use Dajoha\Iter\Modifier\Loop;
use Dajoha\Iter\Modifier\Map;
use Dajoha\Iter\Modifier\Skip;
use Dajoha\Iter\Modifier\Slice;
use Traversable;

/**
 * An abstract iterator which implements all the methods from `IteratorInterface`. This is one of
 * the core classes in this package. Child classes still have to implement php `Iterator` methods.
 * For many cases, `InnerIteratorTrait` can be used for this aim.
 *
 * It allows any child class to implement automatically the following methods:
 *  - Chainable methods for all native *modifier* iterators, and for user/custom iterators as well;
 *  - Find-family methods (`isFound()`, `find()`);
 *  - Index-family methods (`first()`, `last()`, `nth()`);
 *  - Reducing methods:
 *     * Arithmetic reducers (`min()`, `max()`, `sum()`, `average()`);
 *     * Logical reducers (`all()`, `any()`);
 *     * String reducer (`join()`);
 *     * Array reducers (`toValues()`, `toArray()`).
 *     * Counter reducer (`count()`);
 *     * Custom reduce function (`reduce()`);
 *
 * All iterators in this package extend this class.
 *
 * User/custom iterators should as well extend this class (even if it's possible to implement
 * `IteratorInterface` by hand).
 *
 * @see IteratorInterface
 * @see InnerIteratorTrait
 *
 * @template TKey
 * @template TValue
 *
 * @implements IteratorInterface<TKey, TValue>
 */
abstract class AbstractIterator implements IteratorInterface
{
    /**
     * *Warning*: this magic method is very subject to be changed or removed.
     *
     * Allow to call a previously registered user/custom method, thanks to `IteratorRegistry`.
     *
     * @see IteratorRegistry
     *
     * @phpstan-ignore-next-line
     */
    public function __call(string $name, array $arguments)
    {
        $className = IteratorRegistry::getClassName($name);
        if ($className === null) {
            throw new RuntimeException("Didn't find a registered iterator with name \"$name\"");
        }

        return new $className($this, ...$arguments);
    }

    /**
     * @inheritDoc
     */
    public function filter(callable $predicate): Filter
    {
        return new Filter($this, $predicate);
    }

    /**
     * @inheritDoc
     */
    public function filterKeys(callable $predicate): FilterKeys
    {
        return new FilterKeys($this, $predicate);
    }

    /**
     * @inheritDoc
     */
    public function map(callable $mapper): Map
    {
        return new Map($this, $mapper);
    }

    /**
     * @inheritDoc
     */
    public function mapKeys(callable $keyMapper): MapKeys
    {
        return new MapKeys($this, $keyMapper);
    }

    /**
     * @inheritDoc
     */
    public function mapKeyValues(callable $keyValueMapper): MapKeyValues
    {
        return new MapKeyValues($this, $keyValueMapper);
    }

    /**
     * @inheritDoc
     */
    public function skip(int $skip): Skip
    {
        return new Skip($this, $skip);
    }

    /**
     * @inheritDoc
     */
    public function limit(?int $limit = null): Limit
    {
        return new Limit($this, $limit);
    }

    /**
     * @inheritDoc
     */
    public function slice(int $start = 0, ?int $length = null): Slice
    {
        return new Slice($this, $start, $length);
    }

    /**
     * @inheritDoc
     */
    public function chain(iterable|callable ...$iterators): Chain
    {
        return new Chain($this, ...$iterators);
    }

    /**
     * @inheritDoc
     */
    public function cartesian(iterable|callable ...$iterators): Cartesian
    {
        return new Cartesian($this, ...$iterators);
    }

    /**
     * @inheritDoc
     */
    public function zip(iterable|callable ...$iterators): Zip {
        return new Zip($this, ...$iterators);
    }

    /**
     * @inheritDoc
     */
    public function alternate(iterable|callable ...$iterators): Alternate
    {
        return Alternate::new($this, ...$iterators);
    }

    /**
     * @inheritDoc
     */
    public function interleave(iterable|callable $innerIterator): Interleave
    {
        return Interleave::new($this, $innerIterator);
    }

    /**
     * @inheritDoc
     */
    public function interleaveValue(mixed $value = null): Interleave
    {
        return Interleave::new($this, Forever::new($value));
    }

    /**
     * @inheritDoc
     */
    public function flatten(): Flatten
    {
        return new Flatten($this);
    }

    /**
     * @inheritDoc
     */
    public function loop(?int $count = null): Loop {
        return new Loop($this, $count);
    }

    /**
     * @inheritDoc
     * @throws IterException
     */
    public function chunks(int $size): Chunks
    {
        return new Chunks($this, $size);
    }

    /**
     * @inheritDoc
     */
    public function run(?callable $callable): Run {
        return new Run($this, $callable);
    }

    /**
     * @inheritDoc
     */
    public function apply(string $iteratorClass, mixed ...$arguments): IteratorInterface
    {
        return new $iteratorClass($this, ...$arguments);
    }

    /**
     * @inheritDoc
     */
    public function isFound(callable $predicate, mixed &$value = null, mixed &$key = null): bool
    {
        $value = null;
        $key = null;

        while ($this->valid()) {
            $current = $this->current();
            if ($predicate($current)) {
                $value = $current;
                $key = $this->key();
                $this->next();

                return true;
            }
            $this->next();
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function find(callable $predicate, ?bool &$found = null): mixed
    {
        while ($this->valid()) {
            $current = $this->current();
            if ($predicate($current)) {
                $found = true;
                $this->next();

                return $current;
            }
            $this->next();
        }
        $found = false;

        return null;
    }

    /**
     * @inheritDoc
     */
    public function first(?bool &$found = false): mixed
    {
        return $this->valid() ? $this->current() : null;
    }

    /**
     * @inheritDoc
     */
    public function last(?bool &$found = false): mixed
    {
        $value = null;
        $found = false;

        while ($this->valid()) {
            $found = true;
            $value = $this->current();
            $this->next();
        }

        return $value;
    }

    /**
     * @inheritDoc
     *
     * @throws IterException
     */
    public function nth(int $nth, ?bool &$found = false): mixed
    {
        if ($nth < 0) {
            throw new IterException("Negative indices are forbidden: $nth");
        }

        $found = false;

        for ($i = 0; $i !== $nth; ++$i) {
            if (!$this->valid()) {
                return null;
            }
            $this->next();
        }

        if (!$this->valid()) {
            return null;
        }

        $found = true;

        return $this->current();
    }

    /**
     * @inheritDoc
     */
    public function reduce(callable $reducer, mixed $initialValue = null): mixed
    {
        $hasInitialValue = func_num_args() > 1;

        if ($hasInitialValue) {
            $value = $initialValue;
        } else {
            if ($this->valid()) {
                $value = $this->current();
                $this->next();
            } else {
                return null;
            }
        }

        while ($this->valid()) {
            $value = $reducer($value, $this->current());
            $this->next();
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function all(callable $predicate): bool
    {
        while ($this->valid()) {
            if (!$predicate($this->current())) {
                $this->next();
                return false;
            }
            $this->next();
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function any(callable $predicate): bool
    {
        while ($this->valid()) {
            if ($predicate($this->current())) {
                $this->next();
                return true;
            }
            $this->next();
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        $count = 0;
        while ($this->valid()) {
            ++$count;
            $this->next();
        }

        return $count;
    }

    /**
     * @inheritDoc
     */
    public function min(): mixed
    {
        $array = $this->toValues();

        return empty($array) ? null : min($array);
    }

    /**
     * @inheritDoc
     */
    public function max(): mixed
    {
        $array = $this->toValues();

        return empty($array) ? null : max($array);
    }

    /**
     * @inheritDoc
     */
    public function sum(): float|int
    {
        $value = 0;

        while ($this->valid()) {
            $value += $this->current();
            $this->next();
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function average(): float|int|null
    {
        $value = 0;
        $nbValues = 0;

        while ($this->valid()) {
            $value += $this->current();
            ++$nbValues;
            $this->next();
        }

        return $nbValues === 0 ? null : $value / $nbValues;
    }

    /**
     * @inheritDoc
     */
    public function join(string $separator = '', ?string $lastSeparator = null): string
    {
        if (!$this->valid()) {
            return '';
        }

        $value = (string) $this->current();
        $this->next();

        if (!$this->valid()) {
            return $value;
        }

        if ($lastSeparator === null) {
            while ($this->valid()) {
                $value .= $separator . $this->current();
                $this->next();
            }

            return $value;
        }

        while (true) {
            $current = $this->current();
            $this->next();
            if (!$this->valid()) {
                return $value . $lastSeparator . $current;
            }
            $value .= $separator . $current;
        }
    }

    /**
     * @inheritDoc
     */
    public function toValues(): array
    {
        $array = [];

        while ($this->valid()) {
            $array[] = $this->current();
            $this->next();
        }

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $array = [];

        while ($this->valid()) {
            $array[$this->key()] = $this->current();
            $this->next();
        }

        return $array;
    }

    /**
     * @inheritDoc
     */
    public function consume(): void
    {
        while ($this->valid()) {
            $this->current();
            $this->next();
        }
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     *
     * @return Iterator<K, V>
     */
    protected static function toIterator(iterable|callable $iterable): Iterator
    {
        if (is_array($iterable)) {
            return new ArrayIterator($iterable);
        } elseif ($iterable instanceof Iterator) {
            return $iterable;
        } elseif ($iterable instanceof Traversable) {
            $iterator = new IteratorIterator($iterable);
            $iterator->rewind(); // Will not work if "rewind()" is not called. (TODO: find why)

            return $iterator;
        } elseif (is_callable($iterable)) {
            return new Func($iterable);
        }

        // @phpstan-ignore deadCode.unreachable
        throw new LogicException('Unreachable');
    }
}
