<?php

declare(strict_types=1);

namespace Dajoha\Iter\Exception;

use Exception;

class IterException extends Exception
{
}
