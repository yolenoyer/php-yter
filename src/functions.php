<?php

declare(strict_types=1);

use Dajoha\Iter\Iter;
use Dajoha\Iter\IteratorInterface;

if (!function_exists('iter')) {
    /**
     * If the argument is already an `IteratorInterface`, then return it directly. Otherwise,
     * creates a new {@see Iter} instance.
     *
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     *
     * @return IteratorInterface<K, V>
     */
    function iter(iterable|callable $iterable): IteratorInterface
    {
        if ($iterable instanceof IteratorInterface) {
            return $iterable;
        }

        return new Iter($iterable);
    }
}
