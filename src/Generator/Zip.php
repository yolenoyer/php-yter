<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;
use Dajoha\Iter\Iter;
use Iterator;

/**
 * Iterate over multiple iterators simultaneously, each time returning an array containing the
 * current value of each child iterator.
 *
 * @template TValue of mixed[]
 *
 * @extends AbstractIterator<int, TValue>
 */
class Zip extends AbstractIterator
{
    /**
     * @var array|Iterator[]
     */
    protected array $iterators;

    protected int $keyIndex = 0;

    /**
     * @param iterable<mixed>|callable $iterators
     */
    public function __construct(iterable|callable ...$iterators)
    {
        $this->iterators = array_map(fn($it) => self::toIterator($it), $iterators);
    }

    /**
     * @param iterable<mixed>|callable $iterators
     *
     * @return self<mixed[]>
     */
    public static function new(iterable|callable ...$iterators): self
    {
        return new self(...$iterators);
    }

    public function current(): mixed
    {
        return array_map(fn($it) => $it->current(), $this->iterators);
    }

    public function next(): void
    {
        foreach ($this->iterators as $iterator) {
            $iterator->next();
        }

        ++$this->keyIndex;
    }

    public function key(): int
    {
        return $this->keyIndex;
    }

    public function valid(): bool
    {
        return !empty($this->iterators) && Iter::new($this->iterators)->all(fn($it) => $it->valid());
    }

    public function rewind(): void
    {
        foreach ($this->iterators as $iterator) {
            $iterator->rewind();
        }

        $this->keyIndex = 0;
    }
}
