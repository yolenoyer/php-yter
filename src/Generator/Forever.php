<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;

/**
 * Repeat a value forever.
 *
 * Be careful, this iterator **needs** to be stopped in a way or another. For example, the following
 * code will eat memory without limit, until the program crashes: `Forever::new(42)->toValues()`.
 *
 * In order to stop it, you can use for example:
 *  - @see AbstractIterator::limit()
 *  - @see AbstractIterator::slice()
 *  - @see AbstractIterator::zip()
 *
 * @template TValue
 *
 * @extends AbstractIterator<int, TValue>
 */
class Forever extends AbstractIterator
{
    protected int $keyIndex = 0;

    /**
     * @param TValue $value
     */
    public function __construct(protected mixed $value = null)
    {
    }

    /**
     * @template V
     *
     * @param V $value
     *
     * @return Forever<V>
     */
    public static function new(mixed $value = null): Forever
    {
        return new self($value);
    }

    public function current(): mixed
    {
        return $this->value;
    }

    public function next(): void
    {
        ++$this->keyIndex;
    }

    public function key(): int
    {
        return $this->keyIndex;
    }

    public function valid(): bool
    {
        return true;
    }

    public function rewind(): void
    {
        $this->keyIndex = 0;
    }
}
