<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;

/**
 * Iterate over bytes in a string.
 *
 * @extends AbstractIterator<int, string>
 */
class AsciiChars extends AbstractIterator
{
    protected int $index = 0;

    public function __construct(protected string $string)
    {
    }

    public static function new(string $string): self
    {
        return new self($string);
    }

    public function current(): ?string
    {
        return $this->string[$this->index] ?? null;
    }

    public function next(): void
    {
        ++$this->index;
    }

    public function key(): int
    {
        return $this->index;
    }

    public function valid(): bool
    {
        return $this->index < strlen($this->string);
    }

    public function rewind(): void
    {
        $this->index = 0;
    }
}
