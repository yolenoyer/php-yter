<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * An iterator which chains multiple child iterators together.
 *
 * @extends AbstractIterator<mixed, mixed>
 */
class Chain extends AbstractIterator
{
    /** @var Iterator[] */
    protected array $iterators;

    protected bool $isUpToDate = false;

    protected int $iterableIndex = 0;

    protected int $keyIndex = -1;

    protected bool $withOriginalKeys = false;

    /**
     * @param iterable<mixed, mixed>|callable $iterables
     */
    public function __construct(iterable|callable ...$iterables)
    {
        $this->iterators = array_map(
            fn($iterable) => self::toIterator($iterable),
            $iterables,
        );
    }

    /**
     * @param iterable<mixed, mixed>|callable $iterables
     */
    public static function new(iterable|callable ...$iterables): self
    {
        return new self(...$iterables);
    }

    public function withOriginalKeys(bool $withOriginalKeys = true): static
    {
        $this->withOriginalKeys = $withOriginalKeys;

        return $this;
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->isUpToDate) {
            return false;
        }
        $this->isUpToDate = true;

        while (true) {
            $currentIterable = $this->currentIterable();
            if ($currentIterable === null) {
                break;
            }
            if ($currentIterable->valid()) {
                break;
            }
            ++$this->iterableIndex;
        }

        ++$this->keyIndex;

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->currentIterable()?->current();
    }

    public function next(): void
    {
        $this->isUpToDate = false;
        $currentIterable = $this->currentIterable();
        if ($currentIterable === null) {
            return;
        }
        $currentIterable->next();
    }

    public function key(): mixed
    {
        $this->updateIterator();

        return $this->withOriginalKeys ? $this->currentIterable()?->key() : $this->keyIndex;
    }

    public function valid(): bool
    {
        $this->updateIterator();

        return $this->currentIterable()?->valid() ?? false;
    }

    public function rewind(): void
    {
        $this->isUpToDate = false;
        $this->iterableIndex = 0;
        $this->keyIndex = -1;
        foreach ($this->iterators as $iterable) {
            $iterable->rewind();
        }
    }

    public function currentIterable(): ?Iterator
    {
        return $this->iterators[$this->iterableIndex] ?? null;
    }
}
