<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;
use Dajoha\Iter\Iter;
use Dajoha\Iter\Traits\InnerIteratorTrait;

/**
 * Iterate over well-known sequences of characters, like:
 *  - alphabetic letters;
 *  - digits;
 *  - hexadecimal digits;
 *  - base64 letters.
 *
 * @extends AbstractIterator<mixed, string>
 */
class Letters extends AbstractIterator
{
    /** @use InnerIteratorTrait<mixed, string> */
    use InnerIteratorTrait;

    // Sequence codes:
    public const LETTERS_LOWERCASE = 1 << 0;
    public const LETTERS_UPPERCASE = 1 << 1;
    public const DIGITS = 1 << 2;
    public const HEX_DIGITS_UPPERCASE = 1 << 3;
    public const HEX_DIGITS_LOWERCASE = 1 << 4;
    public const BASE64_RFC_4648_URL = 1 << 5;
    public const BASE64_RFC_4648 = 1 << 6;

    // Sequence code aliases:
    public const HEX_DIGITS = self::HEX_DIGITS_UPPERCASE;
    public const BASE64 = self::BASE64_RFC_4648_URL;

    // Unions:
    public const LETTERS = self::LETTERS_LOWERCASE | self::LETTERS_UPPERCASE;
    public const ALL = 0xffffffff;

    // Default sequence:
    public const DEFAULT = self::LETTERS | self::DIGITS;

    // Commonly used strings in sequences definitions below:
    protected const SEQUENCE_LETTERS_UPPERCASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    protected const SEQUENCE_LETTERS_LOWERCASE = 'abcdefghijklmnopqrstuvwxyz';
    protected const SEQUENCE_DIGITS = '0123456789';
    protected const SEQUENCE_HEX_DIGITS_UPPERCASE = '0123456789ABCDEF';
    protected const SEQUENCE_HEX_DIGITS_LOWERCASE = '0123456789abcdef';
    protected const SEQUENCE_BASE64_BASE =
        self::SEQUENCE_LETTERS_UPPERCASE
        . self::SEQUENCE_LETTERS_LOWERCASE
        . self::SEQUENCE_DIGITS;

    // Sequences definitions:
    protected const SEQUENCES = [
        self::LETTERS_UPPERCASE => self::SEQUENCE_LETTERS_UPPERCASE,
        self::LETTERS_LOWERCASE => self::SEQUENCE_LETTERS_LOWERCASE,
        self::DIGITS => self::SEQUENCE_DIGITS,
        self::HEX_DIGITS_UPPERCASE => self::SEQUENCE_HEX_DIGITS_UPPERCASE,
        self::HEX_DIGITS_LOWERCASE => self::SEQUENCE_HEX_DIGITS_LOWERCASE,
        self::BASE64_RFC_4648 => self::SEQUENCE_BASE64_BASE . '+/',
        self::BASE64_RFC_4648_URL => self::SEQUENCE_BASE64_BASE . '-_',
    ];

    public function __construct(int ...$sequences)
    {
        if (empty($sequences)) {
            $sequences = [ self::DEFAULT ];
        }

        $sequenceIterators = Iter::new($sequences)
            ->map(fn($sequence) => AsciiChars::new(self::sequenceToString($sequence)))
        ;

        $this->setInnerIterator(Chain::new(...$sequenceIterators));
    }

    public static function new(int ...$sequences): self {
        return new self(...$sequences);
    }

    protected static function sequenceToString(int $sequence): string
    {
        $string = '';
        $nbSequencesKinds = count(self::SEQUENCES);

        for ($i = 0; $i !== $nbSequencesKinds; ++$i) {
            $bit = 1 << $i;
            if ($sequence & $bit) {
                $string .= self::SEQUENCES[$bit];
            }
        }

        return $string;
    }
}
