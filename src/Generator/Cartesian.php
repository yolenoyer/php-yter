<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * An iterator which performs the cartesian product of child iterators.
 *
 * @extends AbstractIterator<int, list<mixed>>
 */
class Cartesian extends AbstractIterator
{
    /** @var Iterator[] */
    protected array $iterators;

    protected bool $isUpToDate = false;

    protected int $keyIndex = -1;

    /**
     * @phpstan-param iterable<mixed, mixed>|callable $iterable1
     * @phpstan-param iterable<mixed, mixed>|callable $iterables
     */
    public function __construct(iterable|callable $iterable1, iterable|callable ...$iterables)
    {
        array_unshift($iterables, $iterable1);

        $this->iterators = array_map(
            fn($iterable) => self::toIterator($iterable),
            $iterables,
        );
    }

    /**
     * @phpstan-param iterable<mixed, mixed>|callable $iterable1
     * @phpstan-param iterable<mixed, mixed>|callable $iterables
     */
    public static function new(iterable|callable $iterable1, iterable|callable ...$iterables): self
    {
        return new self($iterable1, ...$iterables);
    }

    protected function updateIterator(): void
    {
        if ($this->isUpToDate) {
            return;
        }

        $this->isUpToDate = true;

        $nbIterators = count($this->iterators);
        $first = true;

        for ($i = $nbIterators - 1; $i >= 0; $i--) {
            $iterator = $this->iterators[$i];

            if ($first) {
                $first = false;
            } else {
                $iterator->next();
            }

            if ($iterator->valid() || $i === 0) {
                break;
            }

            $iterator->rewind();
        }

        ++$this->keyIndex;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return array_map(fn($it) => $it->current(), $this->iterators);
    }

    public function next(): void
    {
        $nbIterators = count($this->iterators);
        $lastIterator = $this->iterators[$nbIterators - 1];
        $lastIterator->next();
        $this->isUpToDate = false;
    }

    public function key(): int
    {
        $this->updateIterator();

        return $this->keyIndex;
    }

    public function valid(): bool
    {
        $this->updateIterator();

        foreach ($this->iterators as $iterator) {
            if (!$iterator->valid()) {
                return false;
            }
        }

        return true;
    }

    public function rewind(): void
    {
        $this->isUpToDate = false;
        $this->keyIndex = -1;

        foreach ($this->iterators as $iterator) {
            $iterator->rewind();
        }
    }
}
