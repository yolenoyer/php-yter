<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;

/**
 * Iterate over utf-8 chars in a string.
 *
 * @extends AbstractIterator<int, string>
 */
class Chars extends AbstractIterator
{
    protected int $byteIndex = 0;
    protected int $byteLength;
    protected int $charIndex = 0;
    protected ?int $validatedCharLength = null;

    public function __construct(protected string $string)
    {
        $this->byteLength = strlen($string);
    }

    public static function new(string $string): self
    {
        return new self($string);
    }

    public function current(): string
    {
        return substr($this->string, $this->byteIndex, $this->validatedCharLength);
    }

    public function next(): void
    {
        $this->byteIndex += $this->validatedCharLength;
        ++$this->charIndex;
        $this->validatedCharLength = null;
    }

    public function key(): int
    {
        return $this->charIndex;
    }

    public function valid(): bool
    {
        if (!isset($this->string[$this->byteIndex])) {
            $this->validatedCharLength = null;
            return false;
        }

        $firstByte = ord($this->string[$this->byteIndex]);
        $this->validatedCharLength = self::getCharByteLength($firstByte);
        $remainingByteLength = $this->byteLength - $this->byteIndex;

        return $remainingByteLength >= $this->validatedCharLength;
    }

    public function rewind(): void
    {
        $this->byteIndex = 0;
        $this->validatedCharLength = null;
        $this->charIndex = 0;
    }

    protected static function getCharByteLength(int $firstByte): int
    {
        if ($firstByte < 128) {
            return 1;
        } elseif ($firstByte < 224) {
            return 2;
        } elseif ($firstByte < 240) {
            return 3;
        } else {
            return 4;
        }
    }
}
