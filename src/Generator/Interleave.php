<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;
use Iterator;

/**
 * Interleaves each element of one iterator with each value of another iterator.
 *
 * @template TValueOuter
 * @template TValueInner
 *
 * @extends AbstractIterator<int, TValueOuter|TValueInner>
 */
class Interleave extends AbstractIterator
{
    protected Iterator $outer;

    protected Iterator $inner;

    protected bool $currentIsOuter = true;

    protected bool $first = true;

    protected int $keyIndex = 0;

    /**
     * @phpstan-param iterable<mixed, TValueOuter>|(callable(): TValueOuter) $outer
     * @phpstan-param iterable<mixed, TValueInner>|(callable(): TValueInner) $inner
     */
    public function __construct(iterable|callable $outer, iterable|callable $inner)
    {
        $this->outer = self::toIterator($outer);
        $this->inner = self::toIterator($inner);
    }

    /**
     * @template VOuter
     * @template VInner
     *
     * @phpstan-param iterable<mixed, VOuter>|(callable(): VOuter) $outer
     * @phpstan-param iterable<mixed, VInner>|(callable(): VInner) $inner
     *
     * @return self<VOuter, VInner>
     */
    public static function new(iterable|callable $outer, iterable|callable $inner): self
    {
        return new self($outer, $inner);
    }

    public function current(): mixed
    {
        return $this->getCurrentIterator()->current();
    }

    public function next(): void
    {
        $this->currentIsOuter = !$this->currentIsOuter;
        if (!$this->currentIsOuter) {
            if ($this->first) {
                $this->first = false;
            } else {
                $this->inner->next();
            }
            $this->outer->next();
        }
        $this->keyIndex++;
    }

    public function key(): int
    {
        return $this->keyIndex;
    }

    public function valid(): bool
    {
        if ($this->currentIsOuter) {
            return $this->outer->valid();
        } else {
            return $this->inner->valid() && $this->outer->valid();
        }
    }

    public function rewind(): void
    {
        $this->outer->rewind();
        $this->inner->rewind();
        $this->currentIsOuter = true;
        $this->first = true;
        $this->keyIndex = 0;
    }

    protected function getCurrentIterator(): Iterator
    {
        return $this->currentIsOuter ? $this->outer : $this->inner;
    }
}
