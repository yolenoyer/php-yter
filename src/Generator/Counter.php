<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;

/**
 * A counter iterator.
 *
 * Can provide series of numbers like:
 *  - [0, 1, 2, 3] with `Counter::new(4)`;
 *  - [2, 3, 4] with `Count::from(2, 5)`;
 *  - [100, 110, 120, 130] with `Counter::sized(4, 100, 10)`.
 *
 * @extends AbstractIterator<int, int>
 */
class Counter extends AbstractIterator
{
    protected int $current;

    public function __construct(
        protected ?int $to = null,
        protected int $from = 0,
        protected int $step = 1,
    ) {
        $this->current = $from;
    }

    public static function new(int $to = null, int $from = 0, int $step = 1): self {
        return new self($to, $from, $step);
    }

    public static function from(int $from = 0, int $to = null, int $step = 1): self {
        return new self($to, $from, $step);
    }

    public static function to(int $to = null, int $from = 0, int $step = 1): self {
        return new self($to, $from, $step);
    }

    public static function sized(int $size, int $from = 0, int $step = 1): self {
        $to = $from + $size * $step;

        return new self($to, $from, $step);
    }

    public function current(): int
    {
        return $this->current;
    }

    public function next(): void
    {
        $this->current += $this->step;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return $this->to === null
            || ($this->step < 0 ? ($this->current > $this->to) : ($this->current < $this->to));
    }

    public function rewind(): void
    {
        $this->current = $this->from;
    }
}
