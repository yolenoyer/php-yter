<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;

/**
 * An iterator which returns the result of the given callable forever, by calling it each time.
 *
 * Be careful, this iterator **needs** to be stopped in a way or another.
 *
 * In order to stop it, you can use for example:
 *  - {@see AbstractIterator::limit()}
 *  - {@see AbstractIterator::slice()}
 *  - {@see AbstractIterator::zip()}
 *
 * @template TValue
 *
 * @extends AbstractIterator<int, TValue>
 */
class Func extends AbstractIterator
{
    /** @var callable */
    protected $callable;

    protected bool $isUpToDate = false;

    protected mixed $currentValue;

    protected int $keyIndex = -1;

    /**
     * @param callable(): TValue $callable
     */
    public function __construct(callable $callable)
    {
        $this->callable = $callable;
    }

    /**
     * @template V
     *
     * @param callable(): V $callable
     *
     * @return self<V>
     */
    public static function new(callable $callable): Func
    {
        return new self($callable);
    }

    /**
     * @return bool Return false if update was not needed
     */
    protected function updateIterator(): bool
    {
        if ($this->isUpToDate) {
            return false;
        }
        $this->isUpToDate = true;

        $this->currentValue = ($this->callable)();
        ++$this->keyIndex;

        return true;
    }

    public function current(): mixed
    {
        $this->updateIterator();

        return $this->currentValue;
    }

    public function next(): void
    {
        $this->isUpToDate = false;
    }

    public function key(): int
    {
        $this->updateIterator();

        return $this->keyIndex;
    }

    public function valid(): bool
    {
        return true;
    }

    public function rewind(): void
    {
        $this->isUpToDate = false;
        $this->keyIndex = -1;
    }
}
