<?php

declare(strict_types=1);

namespace Dajoha\Iter\Generator;

use Dajoha\Iter\AbstractIterator;
use Dajoha\Iter\Exception\IterException;
use Iterator;

/**
 * Alternate elements of each iterator.
 *
 * @extends AbstractIterator<int, mixed>
 */
class Alternate extends AbstractIterator
{
    /** @var Iterator[] */
    protected array $iterators;

    /** @var Iterator[] */
    protected array $currentIterators;

    protected int $nbIterators;

    protected int $iteratorIndex = 0;

    protected int $keyIndex = 0;

    /**
     * @param iterable<mixed>|callable $iterators
     */
    public function __construct(iterable|callable ...$iterators)
    {
        $this->iterators = array_values(array_filter(
            array_map(fn($it) => self::toIterator($it), $iterators),
            fn(Iterator $iterator) => $iterator->valid(),
        ));
        $this->currentIterators = $this->iterators;
        $this->updateNbIterators();
    }

    /**
     * @param iterable<mixed>|callable $iterators
     *
     * @return self<mixed>
     */
    public static function new(iterable|callable ...$iterators): self
    {
        return new self(...$iterators);
    }

    public function updateNbIterators(): void
    {
        $this->nbIterators = count($this->currentIterators);
    }

    protected function advanceIteratorIndex(): void
    {
        $this->iteratorIndex = ($this->iteratorIndex + 1) % $this->nbIterators;
    }

    protected function getCurrentIterator(): Iterator
    {
        return $this->currentIterators[$this->iteratorIndex];
    }

    /**
     * @throws IterException
     */
    public function current(): mixed
    {
        if ($this->nbIterators === 0) {
            throw new IterException('No more value to provide');
        }

        return $this->getCurrentIterator()->current();
    }

    public function next(): void
    {
        if ($this->nbIterators === 0) {
            return;
        }

        $this->getCurrentIterator()->next();
        ++$this->keyIndex;

        $this->advanceIteratorIndex();

        while ($this->nbIterators > 0) {
            $this->iteratorIndex %= $this->nbIterators;
            if ($this->getCurrentIterator()->valid()) {
                break;
            }
            array_splice($this->currentIterators, $this->iteratorIndex, 1);
            $this->updateNbIterators();
        }
    }

    public function key(): ?int
    {
        if ($this->nbIterators === 0) {
            return null;
        }

        return $this->keyIndex;
    }

    public function valid(): bool
    {
        return $this->nbIterators !== 0;
    }

    public function rewind(): void
    {
        foreach ($this->iterators as $iterator) {
            $iterator->rewind();
        }
        $this->currentIterators = $this->iterators;
        $this->iteratorIndex = 0;
        $this->keyIndex = 0;
        $this->updateNbIterators();
    }
}
