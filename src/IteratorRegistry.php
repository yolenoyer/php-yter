<?php

declare(strict_types=1);

namespace Dajoha\Iter;

use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * *Warning*: this class is very subject to be changed or removed.
 *
 * A global, static registry of custom iterators. For each registered custom iterator, a custom
 * method is added to any `AbstractIterator`, which allows to chain this custom iterator in the
 * same way as native iterators.
 *
 * @see AbstractIterator
 */
class IteratorRegistry
{
    /** @var string[] */
    protected static array $iterators = [];

    /**
     * Register a new custom iterator.
     *
     * @param class-string<IteratorInterface<mixed, mixed>> $className
     *
     * @throws ReflectionException
     */
    public static function register(string $methodName, string $className): void
    {
        $reflectionClass = new ReflectionClass($className);
        if (!$reflectionClass->implementsInterface(IteratorInterface::class)) {
            throw new RuntimeException("\"$className\" must implement ".IteratorInterface::class);
        }

        self::$iterators[$methodName] = $className;
    }

    /**
     * Return the class name for the given registered method name.
     */
    public static function getClassName(string $methodName): ?string
    {
        return self::$iterators[$methodName] ?? null;
    }

    /**
     * Clear the registered custom iterators.
     */
    public static function clear(): void
    {
        self::$iterators = [];
    }
}
