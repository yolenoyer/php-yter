<?php

declare(strict_types=1);

namespace Dajoha\Iter;

use Dajoha\Iter\Traits\InnerIteratorTrait;

/**
 * A wrapper over any value which is considered to be iterable, which is either:
 *
 *  - An iterable (array, `Iterator`,`IteratorAggregate`, `Traversable);
 *  - Any php callable.
 *
 * It's main goal is to implement {@see IteratorInterface}.
 *
 * *TIP*: A short way to create a `Iter` instance (like with `new Iter(...)`) is to use to the
 * provided function {@see iter()}.
 *
 * @template TKey
 * @template TValue
 *
 * @extends AbstractIterator<TKey, TValue>
 */
class Iter extends AbstractIterator
{
    /** @use InnerIteratorTrait<TKey, TValue> */
    use InnerIteratorTrait;

    /**
     * @phpstan-param iterable<TKey, TValue>|(callable(): TValue) $iterable
     */
    public function __construct(
        iterable|callable $iterable
    ) {
        $this->setInnerIterator($iterable);
    }

    /**
     * @template K
     * @template V
     *
     * @phpstan-param iterable<K, V>|(callable(): V) $iterable
     *
     * @return self<K, V>
     */
    public static function new(iterable|callable $iterable): self
    {
        return new self($iterable);
    }
}
