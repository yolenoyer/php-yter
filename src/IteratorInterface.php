<?php

declare(strict_types=1);

namespace Dajoha\Iter;

use Dajoha\Iter\Generator\Alternate;
use Dajoha\Iter\Generator\Cartesian;
use Dajoha\Iter\Generator\Chain;
use Dajoha\Iter\Generator\Interleave;
use Dajoha\Iter\Modifier\Chunks;
use Dajoha\Iter\Modifier\FilterKeys;
use Dajoha\Iter\Modifier\Flatten;
use Dajoha\Iter\Modifier\MapKeys;
use Dajoha\Iter\Modifier\MapKeyValues;
use Dajoha\Iter\Modifier\Run;
use Iterator;
use Dajoha\Iter\Generator\Zip;
use Dajoha\Iter\Modifier\Filter;
use Dajoha\Iter\Modifier\Limit;
use Dajoha\Iter\Modifier\Loop;
use Dajoha\Iter\Modifier\Map;
use Dajoha\Iter\Modifier\Skip;
use Dajoha\Iter\Modifier\Slice;

/**
 * The base interface implemented by all the iterators in this package.
 *
 * Instead of implementing this interface by hand, it is strongly advised to extend
 * {@see AbstractIterator} instead.
 *
 * @template TKey
 * @template TValue
 *
 * @extends Iterator<TKey, TValue>
 */
interface IteratorInterface extends Iterator
{
    /**
     * Filter the values of the iterator by using a callable: keep only the elements where the
     * predicate returns `true`.
     *
     * @param callable(TValue $value, TKey $key): bool $predicate
     *
     * @return Filter<TKey, TValue>
     */
    public function filter(callable $predicate): Filter;

    /**
     * Filter the keys of the iterator by using a callable: keep only the elements where the
     * predicate returns `true`.
     *
     * @param callable(TKey $key, TValue $value): bool $predicate
     *
     * @return FilterKeys<TKey, TValue>
     */
    public function filterKeys(callable $predicate): FilterKeys;

    /**
     * Replace each value of the iterator by the returned value of the mapper function.
     *
     * @template V
     *
     * @param callable(TValue $value, TKey $key, int $index): V $mapper
     *
     * @return Map<TKey, V>
     */
    public function map(callable $mapper): Map;

    /**
     * Replace each key of the iterator by the returned value of the mapper function.
     *
     * @template K
     *
     * @param callable(TKey $key, TValue $value, int $index): K $keyMapper
     *
     * @return MapKeys<K, TValue>
     */
    public function mapKeys(callable $keyMapper): MapKeys;

    /**
     * Replace each key and value of the iterator by the returned values of the mapper function, in
     * the following way:
     *  - the first item of the returned array becomes the new key;
     *  - the second item of the returned array becomes the new value.
     *
     * @template K
     * @template V
     *
     * @param callable(TKey $key, TValue $value, int $index): list{K, V} $keyValueMapper
     *
     * @return MapKeyValues<K, V>
     */
    public function mapKeyValues(callable $keyValueMapper): MapKeyValues;

    /**
     * Skip the given number of elements at the start of the iterator.
     *
     * @param int $skip Number of elements to skip
     *
     * @return Skip<TKey, TValue>
     */
    public function skip(int $skip): Skip;

    /**
     * Limit the number of elements of the iterator.
     *
     * @param ?int $limit Maximum number of elements; if `null` then no limit is applied.
     *
     * @return Limit<TKey, TValue>
     */
    public function limit(?int $limit = null): Limit;

    /**
     * Skip `$start` elements and limit further elements to a maximum of `$length` elements. It's
     * a shortcut for `$this->skip($skip)->limit($length)`.
     *
     * @param int  $start  Number of elements to skip at the beginning
     * @param ?int $length Maximum number of elements; if `null` then no limit is applied.
     *
     * @return Slice<TKey, TValue>
     */
    public function slice(int $start = 0, ?int $length = null): Slice;

    /**
     * Create an iterator which chains multiple iterators together, by starting with the current
     * one.
     *
     * @param iterable<mixed>|callable $iterators
     */
    public function chain(iterable|callable ...$iterators): Chain;

    /**
     * Create an iterator which performs the cartesian product between current iterator and child
     * iterators.
     *
     * @phpstan-param iterable<mixed, mixed>|callable $iterator1
     * @phpstan-param iterable<mixed>|callable $iterators
     */
    public function cartesian(
        iterable|callable $iterator1,
        iterable|callable ...$iterators,
    ): Cartesian;

    /**
     * Create an iterator which gives array values; the items in the array are the current items of
     * respectively:
     *  - this iterator,
     *  - the iterators given as parameters.
     *
     * The resulting iterator stops providing values whenever at least one sub-iterator reaches its
     * ends; it means that the length of the resulting iterator is the length of the smallest
     * sub-iterator.
     *
     * @param iterable<mixed>|callable $iterators
     *
     * @return Zip<mixed[]>
     */
    public function zip(iterable|callable ...$iterators): Zip;

    /**
     * Alternates elements of each iterator.
     *
     * @param iterable<mixed>|callable $iterators
     */
    public function alternate(iterable|callable ...$iterators): Alternate;

    /**
     * Interleaves each element of the iterator with each value from another iterator.
     *
     * @template VInner
     *
     * @phpstan-param iterable<mixed, VInner>|(callable(): VInner) $innerIterator
     *
     * @return Interleave<TValue, VInner>
     */
    public function interleave(iterable|callable $innerIterator): Interleave;

    /**
     * Interleaves each element of the iterator with the given value.
     *
     * @template VInner
     *
     * @param VInner $value
     *
     * @return Interleave<TValue, VInner>
     */
    public function interleaveValue(mixed $value): Interleave;

    /**
     * Flatten the iterator, by assuming that each element of the iterator must be iterable itself.
     */
    public function flatten(): Flatten;

    /**
     * Loop over the iterator a certain number of times. The parent iterator must be finite,
     * otherwise no loop will never happen.
     *
     * @param ?int $count The number of times to loop the parent iterator. If `null` then loop
     *                    forever.
     *
     * @return Loop<TKey, TValue>
     */
    public function loop(?int $count = null): Loop;

    /**
     * Create an iterator over chunks of $size elements, except the last one which can be smaller.
     * Keys are not preserved.
     *
     * @return Chunks<TValue>
     */
    public function chunks(int $size): Chunks;

    /**
     * Just run the given callable on each item, without altering anything.
     *
     * @param (callable(TValue $value): mixed)|null $callable The return value is not used
     *
     * @return Run<TKey, TValue>
     */
    public function run(?callable $callable): Run;

    /**
     * Wrap the iterator into a custom iterator, by returning `new $iteratorClass($this, ...$args)`.
     *
     * @template TIter of IteratorInterface
     *
     * @param class-string<TIter> $iteratorClass The given class must follow two conditions:
     *                              - It must implement `IteratorInterface`;
     *                              - Its constructor must be of the following form:
     *                                `public function __construct(iterable|callable $iterable, ...$arguments)`
     *
     * @phpstan-return TIter
     */
    public function apply(string $iteratorClass, mixed ...$arguments): IteratorInterface;

    /**
     * Advance the iterator until the given predicate returns `true`.
     *
     * If an item matches the predicate, then:
     *  - the iterator is placed to the next element (ready to search for the next value),
     *  - `$value` and `$key` are filled respectively with the found value and its matching key.
     *
     * If nothing was found, then:
     *  - the iterator reaches its end and is no longer valid (until it is rewound).
     *  - `$value` and `$key` are to `null`.
     *
     * @param callable(TValue $value): bool $predicate Must return `true` when the given value fills
     *                                                 the wanted requirements;
     * @param TValue|null                   $value     This referenced variable is set to the found
     *                                                 value, or `null` if not found;
     * @param TKey|null                     $key       This referenced variable is set to the found
     *                                                 key, or `null` if not found.
     *
     * @return bool Return `true` if a value was found
     */
    public function isFound(callable $predicate, mixed &$value = null, mixed &$key = null): bool;

    /**
     * Advance the iterator until the given predicate returns `true`.
     *
     * If an item matches the predicate, then:
     *  - the iterator is placed to the next element (ready to search for the next value),
     *  - `$found` is set to `true`.
     *
     * If nothing was found, then:
     *  - the iterator reaches its end and is no longer valid (until it is rewound),
     *  - `$found` is set to `false`.
     *
     * @param callable(TValue $value): bool $predicate Must return `true` when the given value fills
     *                                                 the wanted requirements;
     * @param bool                          $found     This referenced variable is set to `true` if
     *                                                 a value was found, `false` otherwise.
     *
     * @return ?TValue Return the found value, or `null` is nothing was found
     */
    public function find(callable $predicate, ?bool &$found = null): mixed;

    /**
     * Return the first value of the iterator. Don't consume the iterator, so this method can be
     * called multiple times while returning the same value.
     *
     * @param bool $found This referenced variable is set to `true` if the value was found, `false`
     *                    otherwise.
     *
     * @return ?TValue
     */
    public function first(?bool &$found = false): mixed;

    /**
     * Return the last value of the iterator. Consume the iterator entirely.
     *
     * @param bool $found This referenced variable is set to `true` if the value was found, `false`
     *                    otherwise.
     *
     * @return ?TValue
     */
    public function last(?bool &$found = false): mixed;

    /**
     * Return the given Nth value of the iterator. Consume the iterator until this item is reached.
     * `$this->nth(0)` can be called multiple times while returning the same value.
     *
     * @param bool $found This referenced variable is set to `true` if the value was found, `false`
     *                    otherwise.
     *
     * @return ?TValue
     */
    public function nth(int $nth, ?bool &$found = false): mixed;

    /**
     * Reduce the iterator to a single value. The `$reducer` callable is called for each item, by
     * giving the previous result to the `$accumulator` parameter.
     *
     * @param callable(mixed $accumulator, TValue $current): mixed  $reducer
     * @param mixed                                                 $initialValue
     */
    public function reduce(callable $reducer, mixed $initialValue = null): mixed;

    /**
     * Check if the given predicate returns `true` for all the iterator values. Consume the iterator
     * entirely.
     *
     * @param callable(TValue $value): bool $predicate
     */
    public function all(callable $predicate): bool;

    /**
     * Check if the given predicate returns `true` for at least one of the iterator values. Consume
     * the iterator entirely.
     *
     * @param callable(TValue $value): bool $predicate
     */
    public function any(callable $predicate): bool;

    /**
     * Return the number of items of the iterator. Consume the iterator entirely.
     */
    public function count(): int;

    /**
     * Return the minimum value of the iterator. Values are compared by using the `min()` php
     * function. Consume the iterator entirely.
     *
     * @see https://www.php.net/manual/en/function.min.php
     */
    public function min(): mixed;

    /**
     * Return the minimum value of the iterator. Values are compared by using the `max()` php
     * function. Consume the iterator entirely.
     *
     * @see https://www.php.net/manual/en/function.max.php
     */
    public function max(): mixed;

    /**
     * Return the sum of the values of the iterator. Consume the iterator entirely.
     */
    public function sum(): float|int;

    /**
     * Return the average of the values of the iterator. Consume the iterator entirely.
     */
    public function average(): float|int|null;

    /**
     * Join all the values of the iterator into a single string. If $lastSeparator is given, then
     * use it to join the two last values. Consume the iterator entirely.
     */
    public function join(string $separator = '', ?string $lastSeparator = null): string;

    /**
     * Return an array of all the values of the iterator. Array keys are not preserved. Consume the
     * iterator entirely.
     *
     * @return list<TValue>
     */
    public function toValues(): array;

    /**
     * Return an array of all the values of the iterator. Array keys are preserved. Consume the
     * iterator entirely.
     *
     * @return array<TKey, TValue>
     */
    public function toArray(): array;

    /**
     * Just consume the iterator entirely. Every side effect of each element will be processed. It's
     * the equivalent of `toValues()`, but nothing is returned.
     */
    public function consume(): void;
}
